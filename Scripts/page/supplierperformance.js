﻿$(document).ready(function () {
    var SupplierPerformanceTask = {
        init: function () {
            var self = this;
            ShowLoading();
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnGenerate = $("#btnGenerate");
        },
        setEvents: function () {
            var self = this;

            self.$btnGenerate.on("click", function (e) {
                e.preventDefault();
                ShowLoading();
                var deferred = $.Deferred(), imageDatas = new Object(), summaryPerRegion = new Object();
                var imgctr = 0, regionlength = $("div[data-regioncontainer-yn=true]").length;
                var regionContainers = $("div[data-regioncontainer-yn=true]");
                var lastregionyn = false;
                var regionCtr = regionContainers.length - 1, regionCurrInd = 0;

                var deferredFunc = function (currDeferred, $element, resoloveyn) {
                    var eliminatedSuppliers = [], eliminateIn12mos = [], getWell = [], locationSupplierCount = [];
                    //var supplierSummaryObj = new Object();

                    if (!$element.hasClass("collapse in")) {
                        $("a[href='#" + $element.attr("id") + "']").trigger("click");
                    }

                    $element.find("a[data-commentbtn-id][data-task-cd=add]").trigger("click");

                    var newImageDatas = new Object();

                    var svgElements = $element.find('svg');

                    $.each(svgElements, function (ind, obj) {
                        var canvas, xml;

                        // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
                        $.each($(this).find('[style*=em]'), function (index, el) {
                            $(this).css('font-size', getStyle(el, 'font-size'));
                        });

                        canvas = document.createElement("canvas");
                        canvas.className = "screenShotTempCanvas";
                        //convert SVG into a XML string
                        xml = (new XMLSerializer()).serializeToString(this);

                        // Removing the name space as IE throws an error
                        xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

                        //draw the SVG onto a canvas
                        canvg(canvas, xml);
                        $(canvas).insertAfter(this);
                        //hide the SVG element
                        $(this).attr('class', 'tempHide');
                        $(this).hide();
                    });

                    html2canvas($element.find("#divSupplierPerformance")[0]).then(function (canvas) {
                        newImageDatas["SupplierPerformance"] = canvas.toDataURL("image/png");
                        html2canvas($element.find("#divCategoryDashboard")[0]).then(function (canvas1) {
                            newImageDatas["CategoryDashboard"] = canvas1.toDataURL("image/png");

                            var getwelllength = $element.find("div[data-getwell-yn]").length;

                            if (getwelllength > 0) {
                                var getwellctr = 1;
                                $.each($element.find("div[data-getwell-yn]"), function (ind, obj) {
                                    var $obj = $(obj);
                                    if ($obj.find("div[data-restricted-yn=true]").length == 0) {
                                        var getwellid = $obj.data("getwell-id");
                                        html2canvas($(obj)[0]).then(function (canvas2) {
                                            var comment = $("div[data-getwellparent-id=" + getwellid + "]").find("textarea").val();
                                            newImageDatas["GetWellScore" + getwellctr] = canvas2.toDataURL("image/png");
                                            newImageDatas["GetWellComment" + getwellctr] = comment.trim();

                                            //newImageDatas["GetWellScore" + getwellctr] = canvas1.toDataURL("image/png");
                                            imageDatas["region" + $element.data("region-id")] = newImageDatas;
                                            if (getwelllength == getwellctr) {
                                                $element.find('.screenShotTempCanvas').remove();
                                                $element.find('.tempHide').show().removeClass('tempHide');
                                                if (regionCurrInd == regionCtr) {
                                                    deferred.resolve();
                                                } else {
                                                    regionCurrInd++;
                                                    deferredFunc(currDeferred, $(regionContainers[regionCurrInd]));
                                                }
                                            }
                                            getwellctr++;
                                        });
                                    } else {
                                        $element.find('.screenShotTempCanvas').remove();
                                        $element.find('.tempHide').show().removeClass('tempHide');
                                        regionCurrInd++;
                                        deferredFunc(currDeferred, $(regionContainers[regionCurrInd]));
                                    }
                                })
                            } else {
                                $element.find('.screenShotTempCanvas').remove();
                                $element.find('.tempHide').show().removeClass('tempHide');
                                imageDatas["region" + $element.data("region-id")] = newImageDatas;
                                if (regionCurrInd == regionCtr) {
                                    currDeferred.resolve();
                                } else {
                                    regionCurrInd++;
                                    deferredFunc(currDeferred, $(regionContainers[regionCurrInd]));
                                }
                            }
                        });
                    });

                }

                deferredFunc(deferred, $(regionContainers[regionCurrInd]));

              
                $.when(deferred).done(function () {
                    var x = imageDatas;
                    $.ajax({
                        url: self.pageUrl + "/ProcessPPT",
                        type: "POST",
                        dataType: 'json',
                        data: {
                            imageStrings: JSON.stringify(x)
                        },
                        success: function (returnData) {
                            console.log(window.downloadPPTURL + returnData.DownloadPath);
                            window.location.href = window.downloadPPTURL + returnData.DownloadPath; 
                            HideLoading();
                        },
                        error: function (data) {
                            var x = data;
                            HideLoading();
                        }
                    });
                });
            });

            $("a[data-commentbtn-id]").on("click", function (e) {
                e.preventDefault();
                var $element = $(this);

                if ($element.data("task-cd") == undefined) {
                    $element.attr("data-task-cd", "add");
                }

                var mode = $element.data("task-cd");

                if (mode == "add") {
                    $element.data("task-cd", "save");
                    $("textarea[data-commenttxt-id=" + $element.data("commentbtn-id") + "]").show();
                    $element.find("span:first").text("Hide Comment");
                } else {
                    $element.data("task-cd", "add");
                    $("textarea[data-commenttxt-id=" + $element.data("commentbtn-id") + "]").hide();
                    $element.find("span:first").text("Show Comment");
                }
            });

            var regionCount = $("div[data-regioncontainer-yn=true]").length;

            $.ajax({
                url: self.pageUrl + "/GetRegionPerformance",
                dataType: "json",
                type: "POST",
                contentType: 'application/json',
                success: function (returnData) {
                    var DefaultPointsObj = [];

                    $.each(returnData.DefaultPoints, function (ind, obj) {
                        var columSeriesObj = {};
                        var fy = obj[0].Value;
                        var fqList = obj[1].Value;
                        $.each(fqList, function (ctr, fq) {
                            DefaultPointsObj.push({ name: fy.FYDescription + " " + fq.FQDescription, "y": 0, "color": "#004b8d" });
                        });
                    })

                    $.each(returnData.Data, function (ind, data) {
                        var $element = $("div[data-regioncontainer-yn=true][data-region-id=" + data.RegionID + "]");
                        var performanceSummary = data.PerformanceSummary;
                        var $suppplierperformancecontainer = $element.find("div[data-supplierperformancediv-yn]");
                        var locations = "";
                        var locationCount = data.Locations.length;

                        $.each(data.Locations, function (ind, obj) {
                            var seperator = locationCount == (ind + 1) ? " & " : ", ";
                            locations += (((locations.length > 0) ? seperator : "") + obj.LocationDescription);
                        })

                        var columnSeriesData = [];

                        var spendAverage = data.Spend.toFixed(0);
                        if (data.RegionSuppliersPerformances.length > 0) {
                            var performanceCount = 0, performanceSum = 0;
                            $.each(data.RegionSuppliersPerformances, function (ind, obj) {
                                var columSeriesObj = {};
                                columnSeriesData.push({ name: obj.FiscalYear.FYDescription + " " + obj.FiscalQuarter.FQDescription, "y": obj.Average, "color": obj.Average >= 85 ? "#004b8d" : "#004b8d" });

                                if (obj.Average > 0) {
                                    performanceCount++;
                                    performanceSum += parseFloat(obj.Average);
                                }
                            })

                            //spendAverage = performanceSum / performanceCount;
                            //spendAverage = isNaN(spendAverage) ? 0 : spendAverage.toFixed(2);
                        } else {
                            columnSeriesData = DefaultPointsObj;
                        }

                        var total = isNaN(data.SupplierCount) ? 0 : data.SupplierCount;//performanceSummary.ExceedsCount + performanceSummary.MeetsCount + performanceSummary.BelowCount + performanceSummary.FailCount;
                        //var title = (data.Region.BuyingRegionID > 0 ? data.Region.RegionDescription + " (" + locations + ")" : "Global");
                        var title = (data.Region.BuyingRegionID > 0 ? locations : "Global");
                        Highcharts.chart($suppplierperformancecontainer.attr("id"), {
                            chart: { type: 'column' },
                            title: {
                                text: "<style='fill:#004b8d;font-weight:700;font-size:21px;font-family:Arial'>" + title + "</style>",
                                align: 'left',
                                x: 30
                            },
                            subtitle: {
                                text: "<style='fill:#004b8d;font-weight:700;font-size:21px;font-family:Arial'>(" + total + " Suppliers / " + spendAverage + "% Spend )</style>",
                                align: 'left',
                                x: 30
                            },
                            xAxis: {
                                type: 'category',
                                labels: {
                                    style: {
                                        fontWeight: '600',
                                        fontSize: '20px',
                                        fontFamily: 'Arial'
                                    }
                                },
                                padding: 2
                            },
                            credits: { enabled: false },
                            yAxis: {
                                gridLineWidth: 0,
                                minorGridLineWidth: 0,
                                title: { text: '' },
                                tickInterval: 20,
                                max: 100,
                                min: 50,
                                labels: {
                                    enabled: false
                                },
                                //dataLabels: {
                                //    enabled:false
                                //},
                            },
                            legend: { enabled: false },
                            plotOptions: {
                                series: {
                                    borderWidth: 0,
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.y:.0f}%',
                                        style: {
                                            fontSize: '22px',
                                            fontWeight: '700',
                                            fontFamily: 'Arial',
                                            color: '#000'
                                        }
                                    }
                                },
                                column: {
                                    pointPadding: 0.15,
                                },
                                bar: {
                                    animation: {
                                        duration: 5000
                                    }
                                }
                            },

                            tooltip: {
                                headerFormat: '<span style="font-size:11px"></span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}%</b> of total<br/>'
                            },

                            "series": [{ "name": "Performances", "data": columnSeriesData }],
                        });

                        $.each(data.PerformanceCategoryDashboards, function (ind, obj) {

                            Highcharts.chart("chartContainer_" + data.RegionID + "_" + obj.SupplierCategory.CategoryName.replace(/\ /g, '_').replace(/\//g, '_'), {
                                //chart: { type: 'column' },                           
                                title: {
                                    text: obj.SupplierCategory.CategoryName, useHTML: true,
                                    style: {
                                        fontWeight: '700',
                                        'border-bottom': '3px solid #666',
                                        fill: '#000',
                                        color: '#000',
                                        //'width': '100%',
                                        textAlign: 'center',
                                    }
                                },
                                subtitle: {
                                    text: obj.SupplierCount + " Suppliers | $" + obj.Expense + "</br>" + " DPO - " + obj.AverageDPO + " Days",
                                    useHTML: true,
                                    style: {
                                        fontWeight: '600',
                                        fill: '#000',
                                        fontSize: '15px',
                                        color: '#000',
                                        'white-space': 'nowrap',
                                        textAlign: 'center',
                                    },
                                },
                                //height: 80,
                                xAxis: {
                                    type: 'category', useHTML: true,
                                    categories: ["<style='color: #332dbd;font-weight:900'>" + obj.Point1Name + "</style>", "<style='color: #11ab13;font-weight:900'>" + obj.Point2Name + "</style>", "<style='color: #48e0dd;font-weight:900'>" + obj.Point3Name + "</style>", "<style='color: #f3901f;font-weight:900'>" + obj.Point4Name + "</style>"],
                                    plotOptions: {
                                        series: {
                                            label: {
                                                connectorAllowed: false
                                            },
                                            pointStart: 84
                                        },
                                    },
                                },
                                credits: { enabled: false },
                                yAxis: {
                                    gridLineWidth: 0,
                                    minorGridLineWidth: 0,
                                    title: { text: '' },
                                    tickInterval: 20,
                                    max: 100,
                                    min: 50,
                                    //height: 50
                                    labels: {
                                        enabled: false
                                    },
                                    //dataLabels: {
                                    //    enabled:false
                                    //},
                                },
                                legend: { enabled: true },
                                plotOptions: {
                                    series: {
                                        //borderWidth: 0,
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.y:.0f}%'
                                        }
                                    },
                                    column: {
                                        pointPadding: -0.25,
                                    }
                                },

                                tooltip: {
                                    headerFormat: '<span style="font-size:11px"></span><br>',
                                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}%</b> of total<br/>'
                                },
                                "series": [{ "name": "Performances", "data": [obj.Point1, obj.Point2, obj.Point3, obj.Point4] }],
                            });
                        });
                    });
                    HideLoading();
                },
                error: function (data) {
                    Alert("Failed to get the data");
                    HideLoading();
                }
            });

            //$.post(self.pageUrl + "/GetRegionPerformance", {  }, function (returnData) {



            //    HideLoading();
            //}, "JSON").fail(function (data) {
            //    alert("error");
            //});
            $("textarea[id^='txtComment']").keyup(function (e) {

                var thisId = this.id.replace("txtComment", "");

                if ($.trim($("textarea[id^='txtComment" + thisId + "']").val()) != null || $.trim($("textarea[id^='txtComment" + thisId + "']").val()) != "") {

                    $.ajax({
                        url: self.pageUrl + "/Post",
                        type: "POST",
                        dataType: 'json',
                        data: {
                            getWellComment: $("textarea[id^='txtComment" + thisId + "']").val(),
                            supplierID: thisId,
                        }
                    });
                }
            });
        },

        getSupplierObj: function () {
            var self = this;

            var supplierObj = {}
            supplierObj.SupplierID = 0;
            supplierObj.ParentSupplierID = 0;
            supplierObj.SupplierName = "";

            return supplierObj;
        },

        getGetWellObj: function () {
            var self = this;

            var getWellObj = {};
            getWellObj.GetWellID = 0;
            getWellObj.SupplierID = 0;
            getWellObj.BusinessUnitID = 0;
            getWellObj.FYID = 0;
            getWellObj.FQID = 0;

            return getWellObj;
        }
    }

    $.fn.supplierPerformanceTask = function () {
        var SupplierPerformanceTaskObj = Object.create(SupplierPerformanceTask);
        SupplierPerformanceTaskObj.init();
    }

    $('body').supplierPerformanceTask();
})


$(function () {
    $('#accordion').on('shown.bs.collapse', function (e) {
        var offset = $('.panel.panel-default > .panel-collapse.in').offset();
        if (offset) {
            $('html,body').animate({
                scrollTop: $('.panel-collapse').siblings('.panel-heading').offset().top
            }, 1000);
        }
    });
});