﻿$(document).ready(function () {
    var SupplierScoreObjTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvent();
        },

        declaration: function () {
            self = this;

            //Maintenance
            self.$scoringTable = $('#scoringTable > tbody tr');
            self.$TScoringHeader = $('#supplierScoringTHeader');
            self.$TScoringData = $('#supplierScoringTBody');
            self.$TScoringFooter = $('#supplierScoringTFoot');
            self.$TScoringParameterDetailHeader = $('#supplierScoringParameterTHeader');
            self.$TScoringParameterDetailData = $('#supplierScoringParameterTBody');
            self.$TScoringParameterDetailFooter = $('#supplierScoringParameterTFoot');
            self.$lblStakeHolder = $('#lblstakeholderID');
            self.$lblSupplierID = $('#lblsupplierID');
            self.$btnSubmit = $('#btnSubmit');
            self.$employeeID = $('#lblstakeholderID');
            self.$dtEliminate
            self.$cbEliminate = $('#cbEliminate');
            self.$cbEliminateNext12Month = $('#cbEliminateNext12Month');
            self.$divdtEliminate = $('#divdtEliminate');

            //variable
            self.$ScoringParameterList = [];
            self.$ScoringParamIDArray = [];
            self.$QuarterHaveValue = [];
            self.$errorMessage = "";
            self.$task = "";
            self.$currentFiscalQuarterID = $('#lblFiscalQuarterID').text();
            self.$paymentTermValue = $("#pNetDaysToPay").text();
            self.$currentFiscalYearID = $('#lblFiscalYearID').text();
            self.$stakeholdersSupplierIDCurrentQuarter = $('#lblstakeholderSupplierID').text();
            self.$BusinessUnitID = $('#lblBusinessUnitID').text();
            self.$isSuccess = true;
            self.$isValidated = false;
            self.$eliminateTask = "";
            

        },

        setEvent: function () {
            var self = this;

                $.each($('#scoringTable > tbody tr'), function (index, element) {
                    var count = index + 1;
                    $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).on('change', function () {
                        $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + count).val(self.computeWeightedScore($('#multiplier' + count).text(), $(this).val()));
                        $('#lblQuarter' + self.$currentFiscalQuarterID + 'Total').text(self.convertDecimalPointToPercentage(self.computeWeightScoreTotalPerQuarter(self.$currentFiscalQuarterID)));
                    });
                });

                self.$cbEliminate.on('change', function () {
                    if (self.$cbEliminate.is(':checked')) {
                        self.$eliminateTask = self.$cbEliminate.val();
                        self.$dtEliminate = $('#dtEliminate').datepicker({
                            dateFormat: "yy-mm-dd",
                            minDate: 'today'
                        });
                        self.$dtEliminate.datepicker('setDate', 'today');
                        self.$cbEliminateNext12Month.prop('disabled', true);
                        self.$divdtEliminate.show();
                    }
                    else {
                        self.$cbEliminateNext12Month.prop('disabled', false);
                    }
                    if (!self.$cbEliminate.is(':checked') && !self.$cbEliminateNext12Month.is(':checked')) {
                        self.$eliminateTask = "";
                        self.$dtEliminate.datepicker('setDate', null);
                        self.$dtEliminate.datepicker("destroy");
                        self.$divdtEliminate.hide();
                    }
                });

                self.$cbEliminateNext12Month.on('change', function () {

                    if (self.$cbEliminateNext12Month.is(':checked')) {
                        var next12MonthDate = self.getNext12MonthDate();

                        self.$eliminateTask = self.$cbEliminateNext12Month.val();
                        self.$dtEliminate = $('#dtEliminate').datepicker({
                            dateFormat: "yy-mm-dd",
                            minDate: new Date(next12MonthDate)

                        });
                        self.$dtEliminate.datepicker('setDate',new Date(next12MonthDate));
                        self.$divdtEliminate.hide();
                        self.$cbEliminate.prop('disabled', true);
                    }
                    else {
                        self.$cbEliminate.prop('disabled', false);
                    }
                    if (!self.$cbEliminate.is(':checked') && !self.$cbEliminateNext12Month.is(':checked')) {
                        self.$eliminateTask = "";
                        self.$dtEliminate.datepicker('setDate',null);
                        self.$dtEliminate.datepicker("destroy");
                        self.$divdtEliminate.hide();
                    }
                });

                
                $("#updateScore").on('click', function () {

                    $.each($('#scoringTable > tbody tr'), function (index, element) {
                        var count = index + 1;
                        if ($('#lblScoringParamName' + count).text() === "Payment Term") {
                            $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val(self.getPaymentTermValue(self.$paymentTermValue));
                            $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + count).val(self.computeWeightedScore($('#multiplier' + count).text(), self.getPaymentTermValue(self.$paymentTermValue)));
                            $('#lblQuarter' + self.$currentFiscalQuarterID + 'Total').text(self.convertDecimalPointToPercentage(self.computeWeightScoreTotalPerQuarter(self.$currentFiscalQuarterID)));
                        }
                        if ($('#lblScoringParamName' + count).text() != "Payment Term") {
                            $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).prop("disabled", false);

                            $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).on('change', function () {
                                $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + count).val(self.computeWeightedScore($('#multiplier' + count).text(), $(this).val()));
                                $('#lblQuarter' + self.$currentFiscalQuarterID + 'Total').text(self.convertDecimalPointToPercentage(self.computeWeightScoreTotalPerQuarter(self.$currentFiscalQuarterID)));
                            });

                            $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + count).val(self.computeWeightedScore($('#multiplier' + count).text(), $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val()));
                            $('#lblQuarter' + self.$currentFiscalQuarterID + 'Total').text(self.convertDecimalPointToPercentage(self.computeWeightScoreTotalPerQuarter(self.$currentFiscalQuarterID)));
                        }
                     
                    });

                    $("#updateScore").hide();
                    $("#saveUpdateScore").show();
                });

                $("#saveUpdateScore").on('click', function (e) {
                    e.preventDefault();

                    self.$task = "update";

                    self.$errorMessage = "";
                    self.$isSuccess = true;

                    ShowLoading();
                    if (self.$eliminateTask != "") {
                        self.Eliminate();
                    }
                    
                    $.each($('#scoringTable > tbody tr'), function (index, element) {
                        var count = index + 1;

                        if ($('#lblScoringParamName' + count).text() != "Payment Term") {
                            var inputScoreValue = $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val();

                            if (inputScoreValue === "" ) {
                                self.$isSuccess = false;
                            }
                        }
                    });

                    if (self.$isSuccess) {
                        $.each($('#scoringTable > tbody tr'), function (index, element) {
                            var count = index + 1;

                            if ($('#lblScoringParamName' + count).text() != "Payment Term") {
                                var inputScoreValue = $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val();
                                self.validateScore(inputScoreValue, $('#lblscoringParamID' + count).text(), count);
                            }
                        });
                    }
                    else {
                        //alert("Please score 1 - 5");
                        Alert("Please fill out the score");
                        HideLoading();
                    }

                });

                self.$btnSubmit.on('click', function (e) {
                    e.preventDefault();
                  
                    self.$task = "add";
                    self.$errorMessage = "";
                    self.$isSuccess = true;
                    var noInputScoreCount = 0;
                    var noInputRemarkCount = 0;
             

                    self.$errorMessage.length = 0;
                    ShowLoading();

                    if (self.$eliminateTask != "") {
                        self.Eliminate();
                    }

                    $.each($('#scoringTable > tbody tr'), function (index, element) {
                        var count = index + 1;
                   

                        if ($('#lblScoringParamName' + count).text() != "Payment Term") {
                            var inputScoreValue = $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val();


                            if (inputScoreValue === "") {
                                noInputScoreCount++;
                            }
          
                          
                        }
                    });
                    if (noInputScoreCount > 0) {
                        self.$errorMessage = "Please fill out the score";
                        self.$isSuccess = false;
                    }
                    else {
                        if(noInputScoreCount > 0)
                            self.$errorMessage = "Please fill out the scores";
                        if(noInputScoreCount > 0)
                            self.$isSuccess = false;
                    }
                 
                    if (self.$isSuccess) {
                        $.each($('#scoringTable > tbody tr'), function (index, element) {
                            var count = index + 1;

                            if ($('#lblScoringParamName' + count).text() != "Payment Term") {
                                var inputScoreValue = $('#q' + self.$currentFiscalQuarterID + 'inputScore' + count).val();
                                self.validateScore(inputScoreValue, $('#lblscoringParamID' + count).text(), count);
                            }
                        });
                    }
                    else {
                        var htmx = "<p>"+self.$errorMessage+"</p>";
                        Alert(htmx);

                        HideLoading();
                    }
                });
        },
        getNext12MonthDate: function(){
            var today = new Date();

            var mm = today.getMonth() + 1;
            var dd = today.getDate();
            var yyyy = today.getFullYear() + 1;

            return  mm + '/' + dd + '/' + yyyy;
            
        },
        getPaymentTermValue: function(paymentTerm){
            if (paymentTerm > 90) {
                return 5;
            }
            else if (paymentTerm >= 75 && paymentTerm <= 90) {
                return 4;
            }
            else if (paymentTerm >= 46 && paymentTerm <= 74) {
                return 3;
            }
            else if (paymentTerm >= 31 && paymentTerm <= 45) {
                return 2;
            }
            else {
                return 1;
            }
        },
        computeWeightScoreTotalPerQuarter: function(quarter){
            var self = this;
            var sum = 0;
            $.each($('#scoringTable > tbody tr'), function (index, element) {
                var count = index + 1;
                sum += parseFloat($('#q' + quarter + 'inputWeightScore' + count).val());
            });
          return  sum / 10;
        },
        addValueOfWeightScoreandTotalPerQuarter: function(){
            var self = this;
            

            for (var countOfQuarter = 0; countOfQuarter < self.$QuarterHaveValue.length; countOfQuarter++) {
                for (countOfScoringParam = 1; countOfScoringParam <= self.$ScoringParameterList.length; countOfScoringParam++) {
                    $('#q' + self.$QuarterHaveValue[countOfQuarter] + 'inputWeightScore' + countOfScoringParam).val(self.computeWeightedScore($('#multiplier' + countOfScoringParam).text(), $('#q' + self.$QuarterHaveValue[countOfQuarter] + 'inputScore' + countOfScoringParam).val()));
                }
                $('#lblQuarter' + self.$QuarterHaveValue[countOfQuarter] + 'Total').text(self.convertDecimalPointToPercentage(self.computeWeightScoreTotalPerQuarter(self.$QuarterHaveValue[countOfQuarter])));
            }

        },
        convertDecimalPointToPercentage: function (decimalPoint) {
            return (decimalPoint * 100).toFixed(0);
        },
        removePercentage:function(multiplier){
            return multiplier / 100;
        },
        computeWeightedScore: function (multiplier, score) {
            var self = this;
            return (score * self.removePercentage(multiplier) * 2).toLocaleString(undefined, {
                minimumFractionDigits: 0,
                maximumFractionDigits: 2
            });
        },
        processOfAddingSupplierScore: function () {
            var self = this;

            $.each($('#scoringTable > tbody tr'), function (index, element) {
                var counter = index + 1;
                $.ajax({
                    url: 'SaveSupplierScore',
                    type: "POST",
                    data: {
                        supplierID: self.$lblSupplierID.text(),
                        employeeID: self.$employeeID.text(),
                        fyID: self.$currentFiscalYearID,
                        fqID: self.$currentFiscalQuarterID,
                        scoringParamID: $('#lblscoringParamID' + counter).text(),
                        score: $('#q' + self.$currentFiscalQuarterID + 'inputScore' + counter).val(),
                        weightedScore: $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + counter).val(),
                        remarks: $('#remarks' + counter).val(),
                        businessUnitID: self.$BusinessUnitID
                    },
                    success: function (resultData) {

                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            });
            self.Assessed();
      
        },
        processOfUpdatingSupplierScore: function () {
            var self = this;
            $.each($('#scoringTable > tbody tr'), function (index, element) {
                var counter = index + 1;
                $.ajax({
                    url: 'UpdateSupplierScore',
                    type: "POST",
                    data: {
                        supplierID: self.$lblSupplierID.text(),
                        fyID: self.$currentFiscalYearID,
                        fqID: self.$currentFiscalQuarterID,
                        scoringParamID: $('#lblscoringParamID' + counter).text(),
                        score: $('#q' + self.$currentFiscalQuarterID + 'inputScore' + counter).val(),
                        weightedScore: $('#q' + self.$currentFiscalQuarterID + 'inputWeightScore' + counter).val()
                    },
                    success: function (resultData) {

                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    }
                });
            });
            self.Assessed();
        },
        validateScore: function (score,scoringParamID,count,task) {

            var self = this;

            $.ajax({
                url: "ValidateScore",
                type: "GET",
                data: {
                    score: score,
                    scoringParameterID: scoringParamID
                },
                success: function (resultData) {
                    if (resultData.successYn) {
                        self.$errorMessage += Object.keys(resultData.msgs).map(function (e) {
                            return resultData.msgs[e] + ", ";
                        });
                    }
                   
                    if (count == $('#scoringTable > tbody tr').length - 1) {
                     
                        setTimeout(function () {
                            if (self.$errorMessage.length > 0) {
                                self.$isSuccess = false;
                            }
                            if (self.$isSuccess) {
                                if (self.$task === "add") {
                                    self.processOfAddingSupplierScore();
                                }
                                if (self.$task === "update") {
                                    self.processOfUpdatingSupplierScore();
                                }

                            }
                            else {
                               
                                var htmx = "<p>Please check the score of the following Category<h5 style='text-align:center'>" + self.$errorMessage.substring(0, self.$errorMessage.length - 2) + "</h5></p>";
                                Alert(htmx)

                                HideLoading();
                            }
                        }, 2000);
                    
                    }
               
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        Eliminate: function(){
            self = this;

            $.ajax({
                url: "Eliminate",
                type: "POST",
                dataType: "json",
                data: {
                    supplierID: self.$lblSupplierID.text(),
                    businessUnitID: self.$BusinessUnitID,
                    dtEliminate: self.$dtEliminate.val(),
                    eliminateTask : self.$eliminateTask
                },
                success: function (resultData) {
                   
                }
            });
        },
        Assessed: function(){
            var self = this;

            $.ajax({
                url: 'Assessed',
                type: "POST",
                data: {
                    id: self.$stakeholdersSupplierIDCurrentQuarter
                },
                success: function (resultData) {
                    
                    setTimeout(function () {
                        HideLoading();
                        Alert();
                        window.location.href = 'Index?supplierID=' + self.$lblSupplierID.text() + '&businessUnitID=' + self.$BusinessUnitID;
                    }, 3000);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        }
    };
    var InitializedSupplierScoreObjTask = function () {
        var supplierScoreObjTask = Object.create(SupplierScoreObjTask);
        supplierScoreObjTask.init();
    };

    InitializedSupplierScoreObjTask();


});

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});