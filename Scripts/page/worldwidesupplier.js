﻿$(document).ready(function () {
    var WorldWideTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;
            self.$mainContainer = $("div[data-maincontainer-yn='true']");
            self.pageUrl = self.$mainContainer.data("page-url");
            self.$btnGenerate = $("#btnGenerate");
            self.$divNumberOfsupplier = self.$mainContainer.find("#numberofsupplier");
            self.$divIndirectPerformance = self.$mainContainer.find("#indirectperformance");

            self.imageData = [];
        },

        setEvents: function () {
            var self = this;
            var deferred = $.Deferred();
            $.get(self.pageUrl + "/GetNumberOfSuppliers", {}, function (data) {
                var seriesData = [];
                $.each(data.NumberOfSuppliers, function (key, obj) {
                    seriesData.push({ name: obj.PointDescription, y: obj.SupplierCount });
                });

                Highcharts.chart('numberofsupplier', {
                    chart: {
                        type: 'column',
                    },
                    title: {
                        text: '',
                        useHTML: true,
                        //style: {
                        //    'width': '100%',
                        //    'text-align': 'start',
                        //    fontSize: '1.8em',
                        //    'line-height': '1.222em',
                        //    fontWeight: '500',
                        //    'border-bottom': '2px solid #004b8d',
                        //    fill: '#000',
                        //    color: '#004b8d',
                        //    'font-family': '"Whitney SSm B", Helvetica, Arial, sans-serif',
                        //},
                    },
                    credits: false,
                    legend: true,
                    xAxis: {
                        type: 'category',
                        labels: {
                            style: {
                                fontWeight: '900'
                            }
                        },
                    },
                    yAxis: {
                        labels: {
                            enabled: false
                        },
                        gridLineWidth: 0,
                        minorGridLineWidth: 0,
                        title: { text: '' }

                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            color: '#004b8d',
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px"></span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
                    },

                    "series": [
                      {
                          "name": "",
                          "colorByPoint": false,
                          "data": seriesData
                      }
                    ],
                });

                deferred.resolve();
            }).fail(function (data) {
                deferred.resolve();
            });

            $.when(deferred).done(function () {
                $.get(self.pageUrl + "/GetWorldwidePerformance", {}, function (data) {
                    var seriesData = [];
                    $.each(data.WorldwidePerformance, function (ind, obj) {
                        var x = obj;
                        seriesData.push({ name: obj.FiscalYear.FYDescription + " " + obj.FiscalQuarter.FQDescription, y: obj.Average });
                    });

                    Highcharts.chart('indirectperformance', {
                        title: {
                            text: '',
                            //useHTML: true,
                            //style: {
                            //    'width': '100%',
                            //    'text-align': 'start',
                            //    fontSize: '1.8em',
                            //    'line-height': '1.222em',
                            //    fontWeight: '500',
                            //    'border-bottom': '2px solid #004b8d',
                            //    fill: '#000',
                            //    color: '#004b8d',
                            //    'font-family': '"Whitney SSm B", Helvetica, Arial, sans-serif',
                            //},
                        },
                        credits: false,
                        legend: true,
                        subtitle: {
                            //text: '<i>* Target 85%</i>',
                            //align: 'left',
                            //x: 40,
                            //y: 45,
                            //style: {
                            //    'width': '100%',
                            //    fontWeight: '700',
                            //},
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style: {
                                    fontWeight: '900'
                                }
                            },
                        },
                        yAxis: {
                            min: 0,
                            max: 100,
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            minorGridLineWidth: 0,
                            title: { text: '' },
                            plotLines: [{
                                color: 'red',
                                dashStyle: 'solid',
                                value: 85,
                                width: 3,
                                label: {
                                    text: '<i>* Target 85%</i>',
                                    align: 'left',
                                    x: 20,
                                    y: 15,
                                    fontWeight: 700,
                                }
                            }]
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                color: '#004b8d',
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.y:.1f}%'
                                }
                            }
                        },

                        tooltip: {
                            headerFormat: '<span style="font-size:11px"></span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                        },

                        "series": [
                          {
                              "name": "",
                              "colorByPoint": false,
                              "data": seriesData
                          }
                        ],
                    });
                });
            });

            self.$btnGenerate.on("click", function (e) {
                e.preventDefault();
                ShowLoading();

                self.setSVG(self.$divNumberOfsupplier[0]);

                html2canvas(self.$divNumberOfsupplier[0]).then(function (canvas) {
                    self.imageData = [];
                    self.imageData.push(canvas.toDataURL("image/png"));
                    self.setSVG(self.$divIndirectPerformance[0]);
                    html2canvas(self.$divIndirectPerformance[0]).then(function (canvas2) {
                        self.imageData.push(canvas2.toDataURL("image/png"));

                        $.ajax({
                            url: self.pageUrl + "/GenerateReport",
                            dataType: "JSON",
                            type: "POST",
                            data: JSON.stringify({ images: self.imageData }),
                            contentType: 'application/json',
                            success: function (returnData) {
                                if (returnData.DownloadPath != undefined && returnData.DownloadPath.length > 0) {
                                    window.location.href = '/WorldwideSUpplier/DownloadPPT?fileinfo=' + returnData.DownloadPath;
                                }
                                HideLoading();
                            },
                            error: function (data) {
                                var x = data;
                                HideLoading();
                            }
                        });
                    });
                });
            });
        },

        setSVG: function (element) {
            var self = this;
            var $element = $(element);
            var svgElements = $element.find('svg');

            $.each(svgElements, function (ind, obj) {
                var canvas, xml;

                // canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
                $.each($(this).find('[style*=em]'), function (index, el) {
                    $(this).css('font-size', getStyle(el, 'font-size'));
                });

                canvas = document.createElement("canvas");
                canvas.className = "screenShotTempCanvas";
                //convert SVG into a XML string
                xml = (new XMLSerializer()).serializeToString(this);

                // Removing the name space as IE throws an error
                xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

                //draw the SVG onto a canvas
                canvg(canvas, xml);
                $(canvas).insertAfter(this);
                //hide the SVG element
                $(this).attr('class', 'tempHide');
                $(this).hide();
            });
        },
    }

    var InitializeWorldWideTask = function () {
        var worldWideTaskObj = Object.create(WorldWideTask);
        worldWideTaskObj.init();
    }

    InitializeWorldWideTask();
});
