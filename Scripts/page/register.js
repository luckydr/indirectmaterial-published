﻿$(document).ready(function () {
    var registerObj = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
        },
        declaration: function () {
            var self = this;

            self.$btnRegister = $("#btnRegister");
            self.$btnCancel = $("#btnCancel");
            self.$frm = $("#frmEmployee");
            self.$registerContent = $("#registerContent");
            self.$pageContent = $("#pageContent");
            self.$successMessage = $("#successMessage");
            self.$errorUL = $("#errorUL");
        },
        setEvents: function () {
            var self = this;

            self.$btnCancel.on("click", function (e) {
                e.preventDefault();
                self.$registerContent.hide();
                self.$pageContent.show();
            });
            self.$btnRegister.on("click", function (e) {
                e.preventDefault();
                ShowLoading();

                $.ajax({
                    url: "/Login/Post",
                    type: "POST",
                    data: self.$frm.serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            Alert("Registration Successful");
                            HideLoading();
                            setTimeout(function () {
                                self.$registerContent.hide();
                                self.$pageContent.show();
                            }, 1000)
                        }
                        else {
                            self.clearValidationError();

                            $.each(response.Errors, function (i, obj) {
                                if (i == "duplicateUsername" || i == "duplicateEmailAddress")   self.$errorUL.append("<li>" + obj + "</li>");
                                else $("#" + i + "Error").text(obj).css({ "color": "red", "font-weight": "normal", "font-size": "smaller" });
                            });

                            HideLoading();
                        }
                    }
                })
            });
        },
        clearValidationError: function () {
            var self = this;

            $.each($('#frmEmployee span[id]'), function (i, obj) {
                $(this).text("");
            });
            self.$errorUL.empty();
        }
    }

    var InitializeRegisterObj = function () {
        var initObj = Object.create(registerObj);
        initObj.init();
    }

    InitializeRegisterObj();
});