﻿$(document).ready(function () {
    $('#supplierPaymentTermsTable').on('draw.dt', function () {
        //$.each($('#supplierPaymentTermsTable tbody').find('button.delete'), function (i, obj) {
        //    $(obj).on('click', function () {
        //        var buttonValue = $(this).val();
        //        SupplierPaymentTermsObjTask.deleteSupplierPaymentTermsData(buttonValue);
        //    })
        //});

        $.each($('#supplierPaymentTermsTable tbody').find('button.edit'), function (i, obj) {
            $(obj).on('click', function () {
                var buttonValue = $(this).val();
                SupplierPaymentTermsObjTask.editSupplierPaymentTermsData(buttonValue);
            })
        });
    });
    var SupplierPaymentTermsObjTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.setDataTableEvents();
        },
        declaration: function () {
            var self = this;

            self.$supplierPaymentTermsDataTable;
            self.$btnAdd = $("#bntAddSupplierPaymentTerms");
            self.$btnSave = $("#samplebtnSave");
            self.$frmSupplierPaymentTerms = $("#frmSupplierPaymentTerms");
            //self.$btnDeleteSupplier = $("#$btnDeleteSupplier");
            self.$tblTBodySupplierPaymentTerms = $('#supplierPaymentTermsTable tbody');
        },

        populateDataInSupplierModal: function (url, deferred) {
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    deferred.resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },

        clearValidationSupplierPaymentTermsForm: function () {
            var self = this;

            $("#successMessage").text("");
            $("#successMessage").removeClass("text-danger text-info");
        },

        editSupplierPaymentTermsData: function (id) {
            var self = this;
            var deferred = $.Deferred();
            $.when(deferred).done(function (partialviewhtmltx) {
                var afterShowEventFunc = [];

                afterShowEventFunc.push(function () {
                    $("#btnYes").on('click', function () {
                        self.clearValidationSupplierPaymentTermsForm();
                        self.processSaveSupplierPaymentTerms();
                    });
                    $("#btnNo").on('click', function () {
                        self.clearValidationSupplierPaymentTermsForm();
                        $('#modal').modal('toggle');
                    });
                });

                $(this).modalTask({
                    title: "Edit Supplier Payment Terms",
                    dataHtml: partialviewhtmltx,
                    eventsAfterShow: afterShowEventFunc
                });
            });
            self.populateDataInSupplierModal('SupplierPaymentTerms/AddEditSupplierPaymentTerms/' + id, deferred);
        },

        processSaveSupplierPaymentTerms: function () {
            var self = this;

            $.ajax({
                url: "/SupplierPaymentTerms/Post",
                type: "POST",
                data: $("#frmSupplierPaymentTerms").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.Data == true) {
                        $("#successMessage").text("Record successfully saved");
                        $("#successMessage").addClass("text-info");
                        $("#supplierPaymentTermsTable").DataTable().destroy();
                        self.setDataTableEvents();
                        self.refreshSupplierPaymentTermsModalForm();
                    }
                    else {
                        var errors = Object.values(response.Errors);
                        errors.map(error => {
                            var li = `<li>${error}</li>`;
                            $("#errorUL").append(li);
                        })
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
        },

        setEvents: function () {
            var self = this;

            //self.$btnAdd.on("click", function (event) {
            //    var deferred = $.Deferred();
            //    $.when(deferred).done(function (partialviewhtmltx) {
            //        var afterShowEventFunc = [];

            //        afterShowEventFunc.push(function () {
            //            $("#btnYes").on('click', function () {
            //                self.clearValidationSupplierPaymentTermsForm();
            //                self.processSaveSupplierPaymentTerms();
            //            });
            //            $("#btnNo").on('click', function () {
            //                self.clearValidationSupplierPaymentTermsForm();
            //                $('#modal').modal('toggle');
            //            });
            //        });
            //        $(this).modalTask({
            //            title: "Add Supplier Payment Terms",
            //            dataHtml: partialviewhtmltx,
            //            eventsAfterShow: afterShowEventFunc
            //        });
            //    });
            //    self.populateDataInSupplierModal('SupplierPaymentTerms/AddEditSupplierPaymentTerms/0', deferred);
            //});
        },

        setDataTableEvents: function () {
            var self = this;

            var deferred = $.Deferred();
            //$.when(deferred).done(function () {
            //    //$.each(self.$tblTBodySupplierPaymentTerms.find('button.delete'), function (i, obj) {
            //    //    $(obj).on('click', function () {
            //    //        var buttonValue = $(this).val();
            //    //        self.deleteSupplierData(buttonValue);
            //    //    })
            //    //});

            //    $.each(self.$tblTBodySupplierPaymentTerms.find('button.edit'), function (i, obj) {
            //        $(obj).on('click', function () {
            //            var buttonValue = $(this).val();
            //            self.editSupplierPaymentTermsData(buttonValue);
            //        })
            //    });
            //});

            //self.buildSupplierPaymentTermsDataTable(deferred);
        },

        refreshSupplierPaymentTermsModalForm: function () {
            var self = this;

            setTimeout(function () {
                $("#cboSupplierMaster").val("");
                $("#cboPaymentTerms").val("");
                $("#cboFiscalYear").val("");
                $("#cboSupplierCategory").val("");
                $("#successMessage").text("");
                $("#modal").modal("toggle");
            }, 500)
        },   
    }

    var InitializedSupplierPaymentTermsObjTask = function () {
        var supplierPaymentTermsObjTask = Object.create(SupplierPaymentTermsObjTask);
        supplierPaymentTermsObjTask.init();
    }

    InitializedSupplierPaymentTermsObjTask();
});

