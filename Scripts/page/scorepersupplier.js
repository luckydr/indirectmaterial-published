﻿(function ($, window, doc) {
    var ScorePerSupplierTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();
        },
        declaration: function () {
            var self = this;

            self.$mainDiv = $("div[data-maincontainer-yn=true]");
            self.pageUrl = GetAppPath() + self.$mainDiv.data("page-url");
            self.taskcd = self.$mainDiv.data("task-cd");

            self.$btnGenerate = self.$mainDiv.find("#btnGenerate");
            self.$btnSend = self.$mainDiv.find("#btnSendReport");
            self.$ulSuppliers = self.$mainDiv.find("ul[data-supplierlist-yn=true]");
            self.$ulSendSuppliers = self.$mainDiv.find("#ulSendSuppliers");
            self.$ulSelectedSuppliers = self.$mainDiv.find("#ulSelectedSuppliers");

            self.$txtSearchSupplier = self.$mainDiv.find("input#txtSearchSupplier");
            self.$btnDownloadScoreDistributionTemplate = $("#btnDownloadScoreDistributionTemplate");
            self.$uploadScoreDistributionFile = $("#uploadScoreDistributionFile");
            self.$ulErrorsScoreDistribution = $("#ulErrorsScoreDistribution");
        },
        setEvents: function () {
            var self = this;

            function hasDuplicates(arr) {
                return new Set(arr).size !== arr.length;
            }

            if (self.taskcd == "view") {
                self.$btnGenerate.on("click", function (e) {
                    var supplierID = parseInt($("#selectSupplier").val());
                    var fyID = parseInt($("#selectYear").val());
                    ShowLoading();
                    self.$btnGenerate.prop("disabled", true);
                    $.get(self.pageUrl + "/GenerateReport", { SupplierID: supplierID, FYID: fyID }, function (returnData) {
                        //alert(URL.createObjectURL(returnData.Data));
                        var url = returnData.Data; //"data:application/pdf;base64," + returnData.Data;
                        $('#embedContainer').find("embed").remove();
                        $('#embedContainer').append("<embed src='" + url + "' width='100%' height='600px'></embed>");
                        //$('#embedContainer').append('<iframe src="' + url + '" height="600px" width="100%"></iframe>')
                        HideLoading();
                        self.$btnGenerate.prop("disabled", false);
                    }).fail(function (data) {
                        var x = "";
                    });
                });
            } else {
                self.$ulSendSuppliers.find("li").off("click").on("click", function () {
                    self.$ulSuppliers.append($(this).prop("outerHTML"));
                    //self.$ulSuppliers.find("li[data-primary-id=" + $(this).data("primary-id") + "]").show();
                    $(this).hide();

                    self.$ulSuppliers.find("li").on("click", function () {
                        //self.$ulSendSuppliers.append($(this).prop("outerHTML"));
                        self.$ulSendSuppliers.find("li[data-primary-id=" + $(this).data("primary-id") + "]").show();
                        //$(this).remove();
                    });
                });


                self.$btnSend.on("click", function (e) {
                    e.preventDefault();
                    if (self.$ulSendSuppliers.find("li").length > 0) {
                        self.$btnSend.prop("disabled", true);
                        var deferred = $.Deferred();

                        $.when(deferred).done(function (answer) {
                            if (answer == "yes") {
                                ShowLoading();
                                var supplierIDs = [];
                                debugger;

                                var fyid = $("#selectYear").val();

                                console.log(fyid);

                                $.each(self.$ulSelectedSuppliers.find("li"), function (ind, obj) {
                                    supplierIDs.push(parseInt($(this).data("primary-id")));
                                });

                                console.log(supplierIDs);

                                $.post(self.pageUrl + "/Sendreport", { supplierIDs: supplierIDs, fyid: fyid }, function (returnJson) {
                                    var listReturnObj = [];
                                    var successCtr = 0, failCtr = 0;
                                    var informationMsg = "Report(s) are now sent";
                                    $.each(returnJson.ResultObj, function (ind, obj) {
                                        var newReturnObj = {};
                                        $.each(obj, function (ind1, obj1) {
                                            newReturnObj[obj1.Key] = obj1.Value;
                                        });
                                        if (newReturnObj.SuccessYN) {
                                            self.$ulSendSuppliers.find("li[data-primary-id=" + newReturnObj.SupplierID + "]").show();
                                            self.$ulSuppliers.find("li[data-primary-id=" + newReturnObj.SupplierID + "]").remove();
                                            successCtr++;
                                        } else {
                                            self.$ulSendSuppliers.find("li[data-primary-id=" + newReturnObj.SupplierID + "]").addClass("failed");
                                            failCtr++;
                                        }

                                        listReturnObj.push(newReturnObj);
                                    });
                                    HideLoading();

                                    if (failCtr > 0 && successCtr > 0) informationMsg = "Some reports were not send successfully";
                                    else if (failCtr > 0 && successCtr == 0) informationMsg = "Cannot send reports";
                                    else if (successCtr == 0 && failCtr == 0) informationMsg = "No report(s) was processed";

                                    Alert(informationMsg);

                                    self.$btnSend.prop("disabled", false);
                                }, "json").fail(function (err) {
                                    HideLoading();
                                    self.$btnSend.prop("disabled", false);
                                    Alert(err.responseText);
                                });
                            } else {
                                self.$btnSend.prop("disabled", false);
                            }
                        });

                        Confirmation("Are you sure you want to send the report?", deferred);
                    }
                    else {
                        var htmx = "<p>Please select supplier</p>";
                        $(this).modalTask({
                            title: "Warning",
                            dataHtml: htmx + "<style>tfooter{display:none} p{text-align: center}</style>",
                            btnYesHideyn: true,
                            btnNoHideyn: true
                        });
                    }

                })
            }
            $.expr[':'].contains = function (a, i, m) {
                return $(a).text().toUpperCase()
                    .indexOf(m[3].toUpperCase()) >= 0;
            };

            self.$txtSearchSupplier.on("keyup", function (e) {
                var $element = $(e.currentTarget);
                var value = $element.val();
                self.$ulSendSuppliers.find("li").removeClass("inactive");
                if (value.length >= 2) {
                    self.$ulSendSuppliers.find("li:not(:contains('" + value + "'))").addClass("inactive");
                }
            });

            self.$btnDownloadScoreDistributionTemplate.on('click', function (e) {
                e.preventDefault();

                window.open(self.pageUrl + "/DownloadScoreDistributionTemplate", "_blank");
            });

            self.$uploadScoreDistributionFile.on('click', function (e) {
                e.preventDefault();

                if ($("#scoreDistributionFile").val() != "") {
                    ShowLoading();
                    var formData = new FormData();
                    formData.append("scoreDistributionExcel", $("#scoreDistributionFile")[0].files[0]);

                    $.ajax({
                        type: "POST",
                        url: self.pageUrl + "/UploadScoreDistribution",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            self.$ulErrorsScoreDistribution.empty();

                            if (response.Dictionaries != null) {
                                ///If theres an error and there is no gathered data
                                $.each(response.Dictionaries, function (index, obj) {

                                    var html = "<li>" + obj + "</li>";
                                    self.$ulErrorsScoreDistribution.append(html);
                                    return false;
                                });
                            }

                            else {

                                if (hasDuplicates(response)) {
                                    var html = "<li>There are duplicate entries on the file. Please check the upload file and try again. </li>";
                                    self.$ulErrorsScoreDistribution.append(html);
                                    console.log(response);
                                    //self.$ulSuppliers.find("li").click();
                                    HideLoading();
                                    return false;
                                }
                                else {
                                    //self.$ulSuppliers.each(function () {
                                    //    $(this).find('li').each(function () {
                                    //        console.log("find li: " + this);
                                    //        $(this).click();
                                    //    });
                                    //});

                                    self.$ulSuppliers.empty();
                                    self.$ulSendSuppliers.find("li").show();
                                    self.$ulErrorsScoreDistribution.empty();
                                    $.each(response, function (index, obj) {
                                        if (self.$ulSendSuppliers.find("li[data-value='" + obj + "']").length) {
                                            self.$ulSendSuppliers.find("li[data-value='" + obj + "']").click();
                                        }
                                        else {
                                            var html = "<li>Some items on the file doesn't exist on the current list. Please check the upload file and try again. </li>";
                                            self.$ulErrorsScoreDistribution.append(html);
                                            //self.$ulSuppliers.find("li").click();
                                            return false;
                                        }
                                        //self.$ulSendSuppliers.find("li[data-value='" + obj + "']").click();
                                    });
                                }


                            }
                            HideLoading();
                        },
                        error: function (response) {
                            HideLoading();
                            Alert("Error uploading data.");
                        }
                    });
                }
                else {
                    Alert("Please enter template.");
                }
            });

        },
    }


    $.fn.initScorePerSupplier = function () {
        var ScorePerSupplierObj = Object.create(ScorePerSupplierTask);
        ScorePerSupplierObj.init();
    }

    $('body').initScorePerSupplier();

})(jQuery, window, document);