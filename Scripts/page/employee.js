﻿$(document).ready(function () {
    var EmployeeObjTask = {
        init: function () {
            var self = this;
            ShowLoading();
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            //Viewing
            self.$employeeDataTable;
            //Maintenance
            self.$btnAdd = $("#btnAddEmployee");
            self.$EmployeeTable = $("#employeeTable");
            self.$btnSave = $("#samplebtnSave");
            self.$frmUser = $("#frmEmployee");
            self.$userStatus = $("#userStatus");
            self.$btnDeleteUser = $("#btnDeleteEmployee");
            self.$userTableDataRow = $('#employeeTable tr');
            self.$tblTBodyEmployee = $('#employeeTable tbody');
           
            //variable
            self.isModalIsActivated = true;
            self.isSuccessyn = false;

            self.deferred = $.Deferred();
        },

        populateDataInEmployeeModal: function (url, deferred) {
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    deferred.resolve(data);
                    
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },

        clearValidationEmployeeForm: function () {
            var self = this;

            $("#validationMessage").text("");
            $("#validationMessage").removeClass("text-danger text-info");
        },

        editEmployeeData: function (id) {
            var self = this;
            var deferred = $.Deferred();
            if (self.isModalIsActivated) {
                $.when(deferred).done(function (partialviewhtmltx) {
                    var afterShowEventFunc = [];
                    var closeModalFunc = [];

                    afterShowEventFunc.push(function () {
                        $("#divUserStatus").show();

                        $("#btnYes").on('click', function () {
                            ShowLoading();
                            self.clearValidationEmployeeForm();
                            self.processSaveEmployee('UpdateEmployee');
                        });
                        $("#btnNo").on('click', function () {
                            self.clearValidationEmployeeForm();
                            $('#employeefrm').modal('toggle');
                        });
                        HideLoading();
                    });
                    closeModalFunc.push(function () {
                        self.isModalIsActivated = true;
                        if (self.isSuccessyn) {
                            self.isSuccessyn = false;
                            var deferredUpdate = $.Deferred();

                            $.when(deferredUpdate).done(function () {
                                location.reload();
                            });
                            Alert("Employee Record have been successfully updated", deferredUpdate,"Information");
                        }
                    });
                    $(this).modalTask({
                        id: "employeefrm",
                        title: "Edit Employee",
                        eventsAfterShow: afterShowEventFunc,
                        eventsCloseModal: closeModalFunc,
                        dataHtml: partialviewhtmltx
                    });
                });
                self.populateDataInEmployeeModal(self.pageUrl + '/AddEditEmployee/' + id, deferred);
            }
        },
        
        
        
        processSaveEmployee: function (method) {
            var self = this;

            $.ajax({
                url: self.pageUrl + "/" + method,
                type: "POST",
                data: $("#frmEmployee").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.successYn) {
                        self.isSuccessyn = true;
                        $('#employeefrm').modal('toggle');
                    }
                    else {
                        $('#validationMessage').append(self.errorMsg(data.msgs));
                        HideLoading();
                    }
                    HideLoading();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        setEvents: function () {
            var self = this;

            
            $.when(self.deferred).done(function () {
                load.reload();
            });

            self.$employeeDataTable = self.$EmployeeTable.dataTable({
                "order": [[0, "desc"]],
                "initComplete": function (settings, json) {
                    HideLoading();
                }
            });


            self.$userTableDataRow.each(function (index, obj) {
                $(this).find("td:first").on('click', function (event) {
                    ShowLoading();
                    event.preventDefault();
                    var value = $(this).text();
                    self.editEmployeeData(value);
                });

            });

            self.$btnAdd.on("click", function (event) {
                event.preventDefault();
                var deferred = $.Deferred();
                if (self.isModalIsActivated) {
                    $.when(deferred).done(function (partialviewhtmltx) {
                        var afterShowEventFunc = [];
                        var closeModalFunc = [];
                        afterShowEventFunc.push(function () {
                            $("#divContactNumber").show();

                            self.isModalIsActivate = false;
                            $("#btnYes").on('click', function () {
                                ShowLoading();
                                self.clearValidationEmployeeForm();
                                self.processSaveEmployee('SaveEmployee');
                            });
                            $("#btnNo").on('click', function () {
                                self.clearValidationEmployeeForm();
                                $('#employeefrm').modal('toggle');
                            });
                        });

                        closeModalFunc.push(function () {
                            self.isModalIsActivated = true;
                            if (self.isSuccessyn) {
                                self.isSuccessyn = false;
                                var deferredAdd = $.Deferred();

                                $.when(deferredAdd).done(function () {
                                    location.reload();
                                });
                                Alert("New employee successfully added", deferredAdd, "Information");
                            }
                        });
                        $(this).modalTask({
                            id: "employeefrm",
                            title: "Add Employee",
                            dataHtml: partialviewhtmltx,
                            eventsAfterShow: afterShowEventFunc,
                            eventsCloseModal: closeModalFunc
                        });
                    });
                    self.populateDataInEmployeeModal(self.pageUrl + '/AddEditEmployee/0', deferred);
                }
              
            });
        },
        errorMsg: function (errorList) {
            errorString = "";
            $.each(errorList, function (index, obj) {
                errorString += '<li style="text-align:start">' + obj + "</li>";
            });
            return errorString;
        },
        refreshEmployeeModalForm: function () {
            var self = this;

            $("#Firstname").val("");
            $("#Lastname").val("");
            $("#Middlename").val("");
            $("#Username").val("");
            $("#EmailAddress").val("");
            $("#cboPositionTitle").val("");
            $("#cboBusinessUnit").val("");
            $("#cboRole").val("");

        }

    };


    var InitializedEmployeeObjTask = function () {
        var employeeObjTask = Object.create(EmployeeObjTask);
        employeeObjTask.init();
    };

    InitializedEmployeeObjTask();
});

$('#usertooltip').tooltip({ boundary: 'window' })

