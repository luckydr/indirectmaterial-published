﻿$(document).ready(function () {
    var uploadTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
        },
        declaration: function () {
            var self = this;

            self.$uploadSupplierMasterFile = $("#uploadSupplierMasterFile");
            self.$uploadSupplierScoreFile = $("#uploadSupplierScoreFile");
            self.$uploadSupplierExpenseFile = $("#uploadSupplierExpenseFile");
            self.$uploadSupplierPaymentTermsFile = $("#uploadSupplierPaymentTermsFile");

            self.$btnDownloadSupplierTemplate = $("#btnDownloadSupplierTemplate");
            self.$btnDownloadSupplierScoreTemplate = $("#btnDownloadSupplierScoreTemplate");
            self.$btnDownloadSupplierExpenseTemplate = $("#btnDownloadSupplierExpenseTemplate");
            self.$btnDownloadSupplierPaymentTermsTemplate = $("#btnDownloadSupplierPaymentTermsTemplate");

            self.$ulErrorsSupplierMaster = $("#ulErrorsSupplierMaster");
            self.$ulErrorsSupplierScore = $("#ulErrorsSupplierScore");
            self.$ulErrorsSupplierExpense = $("#ulErrorsSupplierExpense");
            self.$ulErrorsSupplierPaymentTerms = $("#ulErrorsSupplierPaymentTerms");

            self.$selectfyid = $("select#fyid");
            self.$selectfqid = $("select#fqid");
            
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
        },
        setEvents: function () {
            var self = this;

            self.$uploadSupplierMasterFile.on('click', function (e) {
                e.preventDefault();

                if ($("#supplierMasterFile").val() != "") {
                    ShowLoading();
                    var formData = new FormData();
                    formData.append("supplierMasterExcel", $("#supplierMasterFile")[0].files[0]);

                    $.ajax({
                        type: "POST",
                        url: self.pageUrl + "/UploadSupplier",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            self.$ulErrorsSupplierMaster.empty();
                            $.each(response.Dictionaries, function (index, obj) {;
                                var html = "<li>" + obj + "</li>";
                                self.$ulErrorsSupplierMaster.append(html);
                            });
                            HideLoading();
                        },
                        error: function (response) {
                            HideLoading();
                            Alert("Error uploading data.");
                        }
                    });
                }
                else {
                    Alert("Please enter template.");
                }
            });

            self.$uploadSupplierScoreFile.on('click', function (e) {
                var deferred = $.Deferred();
                e.preventDefault();

                $.when(deferred).done(function (answer) {
                    if (answer == "yes") {
                        if ($("#supplierScoreFile").val() != "") {
                            ShowLoading();
                            var formData = new FormData();
                            formData.append("supplierScoreExcel", $("#supplierScoreFile")[0].files[0]);
                            formData.append("fyid", self.$selectfyid.val());
                            formData.append("fqid", self.$selectfqid.val());

                            $.ajax({
                                type: "POST",
                                url: self.pageUrl + "/UploadSupplierScore",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    self.$ulErrorsSupplierScore.empty();
                                    $.each(response.Dictionaries, function (index, obj) {
                                        var html = "<li>" + obj + "</li>";
                                        self.$ulErrorsSupplierScore.append(html);
                                    });
                                    HideLoading();
                                },
                                error: function (response) {
                                    HideLoading();
                                    Alert("Error uploading data.");
                                }
                            });
                        }
                        else {
                            Alert("Please enter template.");
                        }
                    }
                });
                self.changeSelectedAttribute();
                Confirmation("Are you sure you want to upload Supplier Score for Fiscal Quarter " + $("#fqid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('q')[1] + " of Fiscal Year " + $("#fyid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('fy')[1] + "?", deferred);
            });

            self.$uploadSupplierExpenseFile.on('click', function (e) {
                var deferred = $.Deferred();
                e.preventDefault();

                $.when(deferred).done(function (answer) {
                    if (answer == "yes") {
                        if ($("#supplierExpenseFile").val() != "") {
                            ShowLoading();
                            var formData = new FormData();
                            formData.append("supplierExpenseExcel", $("#supplierExpenseFile")[0].files[0]);
                            formData.append("fyid", self.$selectfyid.val());
                            formData.append("fqid", self.$selectfqid.val());

                            $.ajax({
                                type: "POST",
                                url: self.pageUrl + "/UploadSupplierExpense",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    self.$ulErrorsSupplierExpense.empty();
                                    $.each(response.Dictionaries, function (index, obj) {
                                        var html = "<li>" + obj + "</li>";
                                        self.$ulErrorsSupplierExpense.append(html);
                                    });
                                    HideLoading();
                                },
                                error: function (response) {
                                    HideLoading();
                                    Alert("Error uploading data.");
                                }
                            });
                        }
                        else {
                            Alert("Please enter template.");
                        }
                    }
                });
                self.changeSelectedAttribute();
                Confirmation("Are you sure you want to upload Supplier Expense for Fiscal Quarter " + $("#fqid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('q')[1] + " of Fiscal Year " + $("#fyid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('fy')[1] + "?", deferred);
            });

            self.$uploadSupplierPaymentTermsFile.on('click', function (e) {
                var deferred = $.Deferred();
                e.preventDefault();

                $.when(deferred).done(function (answer) {
                    if (answer == "yes") {
                        if ($("#supplierPaymentTermsFile").val() != "") {
                            ShowLoading();
                            var formData = new FormData();
                            formData.append("supplierPaymentTermsExcel", $("#supplierPaymentTermsFile")[0].files[0]);
                            formData.append("fyid", self.$selectfyid.val());
                            formData.append("fqid", self.$selectfqid.val());

                            $.ajax({
                                type: "POST",
                                url: self.pageUrl + "/UploadSupplierPaymentTerms",
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (response) {
                                    self.$ulErrorsSupplierPaymentTerms.empty();
                                    $.each(response.Dictionaries, function (index, obj) {
                                        var html = "<li>" + obj + "</li>";
                                        self.$ulErrorsSupplierPaymentTerms.append(html);
                                    });
                                    HideLoading();
                                },
                                error: function (response) {
                                    HideLoading();
                                    Alert("Error uploading data.");
                                }
                            });
                        }
                        else {
                            Alert("Please enter template.");
                        }
                    }
                });
                self.changeSelectedAttribute();
                Confirmation("Are you sure you want to upload Supplier Payment Terms for Fiscal Quarter " + $("#fqid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('q')[1] + " of Fiscal Year " + $("#fyid").find("option[selected='selected']")[0].innerHTML.toLowerCase().split('fy')[1] + "?", deferred);
            });
            self.$btnDownloadSupplierTemplate.on('click', function (e) {
                e.preventDefault();
                
                window.open(self.pageUrl + "/DownloadSupplierTemplate", "_blank");
            });

            self.$btnDownloadSupplierScoreTemplate.on('click', function (e) {
                e.preventDefault();

                window.open(self.pageUrl + "/DownloadSupplierScoreTemplate", "_blank");
            });

            self.$btnDownloadSupplierExpenseTemplate.on('click', function (e) {
                e.preventDefault();

                window.open(self.pageUrl + "/DownloadSupplierExpenseTemplate", "_blank");
            });

            self.$btnDownloadSupplierPaymentTermsTemplate.on('click', function (e) {
                e.preventDefault();

                window.open(self.pageUrl + "/DownloadSupplierPaymentTermsTemplate", "_blank");
            });
        },
        changeSelectedAttribute: function () {
            if ($("#fyid").val() != $("#fyid").find("option[selected='selected']").val()) {
                $("#fyid").find("option[selected='selected']").removeAttr("selected");
                $("#fyid").find('option[value=' + $("#fyid").val() + ']').attr("selected", true);
            }
            if ($("#fqid").val() != $("#fqid").find("option[selected='selected']").val()) {
                $("#fqid").find("option[selected='selected']").removeAttr("selected");
                $("#fqid").find('option[value=' + $("#fqid").val() + ']').attr("selected", true);
            }
        }
    }
    var InitializeUploadTask = function () {
        var uploadTaskObj = Object.create(uploadTask);
        uploadTaskObj.init();
    }
    InitializeUploadTask();
});