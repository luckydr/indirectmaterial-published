﻿$(document).ready(function () {
    var ChartTaskObj = {
        init: function () {
            var self = this;
            //ShowLoading();
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;
            self.$btnProcess = $("#btnProcess");
            self.charts = [];
        },

        setEvents: function () {
            var self = this;
            self.$btnProcess.on("click", function () {
                ShowLoading();
                var dataHtml = "<form id='frmExport' name='frmExport' method='post' enctype= 'multipart/form-data'>" +
                              " <div class='SE004-search-box-inline inline-search padded-container'>" +
                              "     <div class='row'>" +
                              "         <div class='col-xs-7 col-sm-12'>"+
                              "             <label for='' class='cm-search__label sr-only'></label>"+
                              "             <input class='search_input' id='txtNoPerSlide' type='text' name='noOfPerSlide' value='1' placeholder='No. of Images per slide' style='text-align: center;' autocomplete='off'>" +
                              "         </div>"+
                              "     </div>"+
                              " </div>"
                              "</form>";


                var eventsAfterShow = [];

                eventsAfterShow.push(function (modalObj) {
                    modalObj.$modalBtnYes.on("click", function () {
                        var charts = [];
                        var images = [];
                        if (self.charts.length > 0) {
                            var fileData = new FormData($("form#frmExport")[0]);  //$("form#frmExport").serialize();
                            var mainDeferred = $.Deferred();

                            $.each(self.charts, function (ind, chart) {
                                var deferred = $.Deferred();
                                $.when(deferred).done(function (imgFile) {
                                    images.push(imgFile);
                                    var ctr = images.length;
                                    fileData.append("image" + ctr, imgFile);
                                    if (self.charts.length == ctr) {
                                        mainDeferred.resolve();
                                    }
                                });

                                self.processChartImages(chart, deferred);
                            });
                            
                            $.when(mainDeferred).done(function () {
                                if (images.length > 0) {
                                    //fileData.append("noOfPerSlide", 2);
                                    $.ajax({
                                        url: '/Chart/ProcessPPT',
                                        type: "POST",
                                        contentType: false, // Not to set any content header  
                                        processData: false, // Not to process data  
                                        data: fileData,
                                        success: function (result) {
                                            window.location.href = result.FilePath;
                                        },
                                        error: function (err) {
                                            var htmx = "<p>Error</p>";
                                            $(this).modalTask({
                                                title: "Warning",
                                                dataHtml: htmx + "<style>.modal-footer{display:none} p{text-align: center}</style>",
                                                btnYesHideyn: true,
                                                btnNoHideyn: true
                                            });

                                            //alert("error");
                                        }
                                    });
                                    //$.post("/PPT/ProcessPPT", {}, function (data) {
                                    //    alert("IV");
                                    //}).fail(function () {
                                    //    alert("X");
                                    //});
                                    //$.each(images, function () {

                                    //});                                
                                }
                            });
                        }
                    });

                    modalObj.$modalForm.find("input#txtNoPerSlide").on("change", function (e) {
                        var curval = $(this).val();

                        if (!isNaN(parseInt(curval))) {
                            var intval = parseInt(curval);

                            if (intval < 1) {
                                $(this).val(1);
                            }
                        } else {
                            $(this).val("1");
                        }
                    })
                });

                $(this).modalTask({
                    id: "modalExportCharts",
                    title: "Export Charts To PPT",
                    dataHtml: dataHtml,
                    width: 25,
                    btnYestx: "Export",
                    btnNoHideyn: true,
                    //height: undefined,
                    eventsAfterShow: eventsAfterShow,
                    eventsCloseModal: [],
                });
            });

            self.initHighChart("container1");
            self.initHighChart2("container2");
        },

        processChartImages: function (chart, deferred) {
            var self = this;

            var svg = chart.getSVG();
            var canvas = document.createElement('canvas');
            canvas.width = $(chart.container).width();
            canvas.height = $(chart.container).height();
            var ctx = canvas.getContext('2d');

            var img = document.createElement('img');

            img.onload = function () {
                ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);

                var dataURI = canvas.toDataURL('image/png');

                var byteString = atob(dataURI.split(',')[1]);
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                // write the bytes of the string to an ArrayBuffer
                var ab = new ArrayBuffer(byteString.length);
                var ia = new Uint8Array(ab);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }

                var imgfile = new Blob([ab], { type: mimeString });

                deferred.resolve(imgfile);
            };

            img.setAttribute('src', 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg))));

            //var byteString = atob(dataURI.split(',')[1]);
            //var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            //// write the bytes of the string to an ArrayBuffer
            //var ab = new ArrayBuffer(byteString.length);
            //var ia = new Uint8Array(ab);
            //for (var i = 0; i < byteString.length; i++) {
            //    ia[i] = byteString.charCodeAt(i);
            //}

            //var imgfile = new Blob([ab], { type: mimeString });
            //return imgfile;
            //var fileData = new FormData();
            //fileData.append("test", imgfile);

            //$.ajax({
            //    url: '/PPT/UploadFiles',
            //    type: "POST",
            //    contentType: false, // Not to set any content header  
            //    processData: false, // Not to process data  
            //    data: fileData,
            //    success: function (result) {
            //        alert(result);
            //    },
            //    error: function (err) {
            //        alert(err.statusText);
            //    }
            //});
        },

        initHighChart: function (container) {
            var self = this;

            var saveChartImg = function (chart, callback) {
                var svg = chart.getSVG();
                var canvas = document.createElement('canvas');
                canvas.width = $(chart.container).width();
                canvas.height = $(chart.container).height();
                var ctx = canvas.getContext('2d');

                var img = document.createElement('img');

                img.onload = function () {
                    ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
                    callback(canvas.toDataURL('image/png'));
                };

                img.setAttribute('src', 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svg))));
            };

            self.charts.push(Highcharts.chart(container, {

                title: {
                    text: 'Solar Employment Growth by Sector, 2010-2016'
                },

                subtitle: {
                    text: 'Source: thesolarfoundation.com'
                },

                yAxis: {
                    title: {
                        text: 'Number of Employees'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },

                series: [{
                    name: 'Installation',
                    data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                }, {
                    name: 'Manufacturing',
                    data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                }, {
                    name: 'Sales & Distribution',
                    data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                }, {
                    name: 'Project Development',
                    data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                }, {
                    name: 'Other',
                    data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                },

                exporting: {
                    menuItemDefinitions: {
                        // Custom definition
                        test: {
                            onclick: function () {
                                saveChartImg(this, function (dataURI) {
                                    var byteString = atob(dataURI.split(',')[1]);
                                    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                                    // write the bytes of the string to an ArrayBuffer
                                    var ab = new ArrayBuffer(byteString.length);
                                    var ia = new Uint8Array(ab);
                                    for (var i = 0; i < byteString.length; i++) {
                                        ia[i] = byteString.charCodeAt(i);
                                    }

                                    var imgfile = new Blob([ab], { type: mimeString });
                                    var fileData = new FormData();
                                    fileData.append("test", imgfile);

                                    $.ajax({
                                        url: '/PPT/UploadFiles',
                                        type: "POST",
                                        contentType: false, // Not to set any content header  
                                        processData: false, // Not to process data  
                                        data: fileData,
                                        success: function (result) {
                                            alert(result);
                                        },
                                        error: function (err) {
                                            alert(err.statusText);
                                        }
                                    });
                                });
                                //this.renderer.label(
                                //    'You just clicked a custom menu item',
                                //    100,
                                //    100
                                //)
                                //.attr({
                                //    fill: '#a4edba',
                                //    r: 5,
                                //    padding: 10,
                                //    zIndex: 10
                                //})
                                //.css({
                                //    fontSize: '1.5em'
                                //})
                                //.add();
                            },
                            text: 'Download PowerPoint'
                        }
                    },
                    buttons: {
                        contextButton: {
                            menuItems: ['downloadPNG', 'downloadSVG', 'separator', 'test']
                        }
                    }
                }
            }));
        },

        initHighChart2: function (container) {
            var self = this;

            $.getJSON("https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/usdeur.json", function (data) {
                self.charts.push(Highcharts.chart(container, {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: 'USD to EUR exchange rate over time'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Exchange rate'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },

                    series: [{
                        type: 'area',
                        name: 'USD to EUR',
                        data: data
                    }]
                }));
            });
        }
    }

    var InitializeChartTask = function () {
        var chartTaskObj = Object.create(ChartTaskObj);
        chartTaskObj.init();
    }

    InitializeChartTask();
});