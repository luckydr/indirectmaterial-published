﻿$(document).ready(function () {
    var UserLogsObjTask = {
        init: function () {
            var self = this;
            ShowLoading();
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            //Viewing
            self.$userLogsDataTable;
            //Maintenance
            self.$UserLogsTable = $("#userLogsTable");
            self.$userLogsTableDataRow = $('#userLogsTable tr');
            self.$tblTBodyUserLogs = $('#userLogsTable tbody');
           
            //variable
            self.isSuccessyn = false;

            self.deferred = $.Deferred();
        },

        setEvents: function () {
            var self = this;

            
            $.when(self.deferred).done(function () {
                load.reload();
            });

            self.$userLogsDataTable = self.$UserLogsTable.dataTable({
                "order": [[0, "desc"]],
                "initComplete": function (settings, json) {
                    HideLoading();
                }
            });
        },
        errorMsg: function (errorList) {
            errorString = "";
            $.each(errorList, function (index, obj) {
                errorString += '<li style="text-align:start">' + obj + "</li>";
            });
            return errorString;
        },

    };


    var InitializedUserLogsObjTask = function () {
        var userLogsObjTask = Object.create(UserLogsObjTask);
        userLogsObjTask.init();
    };

    InitializedUserLogsObjTask();
});

$('#usertooltip').tooltip({ boundary: 'window' })

