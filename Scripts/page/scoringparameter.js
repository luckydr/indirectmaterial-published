﻿$(document).ready(function () {
    var ScoringParameterObjTask = {
        init: function () {
            var self = this;

            ShowLoading();
            self.declaration();
            self.setEvent();

        },
        declaration: function () {
            var self = this;


            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$dataTableScoringParam;
            self.$scoringParam = $('#tblScoringParam');
            self.$btnAdd = $('#btnAddScoringParam');
            self.isModalIsActivated = true;
            self.isSuccessyn = false;
            self.$scoringParamRow = $('#tblScoringParam tr');
            self.$scoringParamTBody = $('#tblScoringParam tbody');
            self.$paramStatus = $('#ParamStatus');
            self.$scoringParamBUMultiplierTable = $('#scoringParamBUMultiplierTable');
            self.$btnScoringParamBUMultiplierAddEdit = $('#btnAddSaveScoringParamBUMultiplier');



            //$('#validationMessage') = $('#validationMessage');
        },
        editQuestionnaireData: function(id){
            var self = this;
            var deferred = $.Deferred();
            if (self.isModalIsActivated) {
                $.when(deferred).done(function (partialviewhtmltx) {
                    var afterShowEventFunc = [];
                    var closeModalFunc = [];
                    afterShowEventFunc.push(function () {
                        self.isModalIsActivate = false;

                        //declaration

                        self.btnUpdateScoringParam = $('#btnUpdateScoringParam');

                        //Multiplier Elements
                        self.$btnAddSaveScoringParamBUMultiplier = $('#btnAddSaveScoringParamBUMultiplier');
                        self.$scoringParamMultiplierTableContainer = $('#scoringParamMultiplierTableContainer');
                        self.$scoringParamMultiplierFormContainer = $('#scoringParamMultiplierFormContainer');
                        self.$addsavecaption = $('#addsavecaption');
                        self.$btnCancelContainer = $('#btnCancelContainer');
                        self.$btnCancelScoringParamBUMultiplier = $('#btnCancelScoringParamBUMultiplier');

                        //Tabs Elements
                        self.$scroingParameterButtonTab = $('#scoringParameterButtonTab');
                        self.$scoringParameterDetailsButtonTab = $('#scoringParameterDetailsButtonTab');
                        self.$scoringParameterMultiplerButtonTab = $('#scoringParameterMultiplerButtonTab');


                        //tabs event
                        self.$scroingParameterButtonTab.click(function (e) {
                            e.preventDefault();
                            self.initializeTabEvents("scoringParameter");
                        });
                        self.$scoringParameterDetailsButtonTab.click(function (e) {
                            e.preventDefault();
                            self.initializeTabEvents("scoringParameterDetails");
                        });
                        self.$scoringParameterMultiplerButtonTab.click(function (e) {
                            e.preventDefault();
                            self.initializeTabEvents("scoringParameterMultipler");
                        });



                        //multiplier events
                        self.$btnAddSaveScoringParamBUMultiplier.on("click", function (event) {
                            if (self.$addsavecaption.text() === "New Multiplier") {
                                self.$scoringParamMultiplierTableContainer.hide();
                                self.$scoringParamMultiplierFormContainer.show();
                                self.$btnCancelContainer.show();
                                self.$addsavecaption.text("Save Multiplier");
                            }
                            else {
                                $("#validationMessageScoringParameterMultiplier").empty();
                                ShowLoading();
                                self.processSaveScoringParameterMultiplier();
                            }

                        });

                        self.$btnCancelScoringParamBUMultiplier.on("click", function (event) {
                            $("#validationMessage").empty();
                            self.$scoringParamMultiplierTableContainer.show();
                            self.$scoringParamMultiplierFormContainer.hide();
                            self.$btnCancelContainer.hide();
                            self.$addsavecaption.text("New Multiplier");
                        });

                        $('#scoringParamBUMultiplierTable').DataTable();


                        


                        self.btnUpdateScoringParam.on('click', function () {
                            ShowLoading();
                            $('#validationMessageScoringParameter').empty();
                            self.processSaveScoringParameter('UpdateScoringParameter');
                        });
                        //$("#btnNo").on('click', function () {
                        //    $('#surveyfrm').modal('toggle');
                        //});
                    });
                    closeModalFunc.push(function () {
                        self.isModalIsActivate = true;
                        if (self.isSuccessyn) {
                            self.isSuccessyn = false;
                            var deferredEdit = $.Deferred();

                            $.when(deferredEdit).done(function () {
                                location.reload();
                            });
                            Alert("Scoring Parameter changes has been saved.", deferredEdit, "Information");
                        }
                    });

                    $(this).modalTask({
                        id: "surveyfrm",
                        title: "Edit Survey",
                        dataHtml: partialviewhtmltx,
                        eventsAfterShow: afterShowEventFunc,
                        eventsCloseModal: closeModalFunc,
                        btnYesHideyn: true,
                        btnNoHideyn: true
                    });
                });
                self.populateDataInScoringParameterModal(self.pageUrl + '/AddEditScoringParameter/'+id, deferred);
            }
        },
        
        setEvent: function () {
            var self = this;

            self.$dataTableScoringParam = self.$scoringParam.dataTable({
                "order": [[1, "desc"]],
                "initComplete": function (settings, json) {
                    HideLoading();
                }
            });

     

            self.$btnAdd.on("click", function (event) {

                var deferred = $.Deferred();
                if (self.isModalIsActivated) {
                    $.when(deferred).done(function (partialviewhtmltx) {
                        var afterShowEventFunc = [];
                        var closeModalFunc = [];
                        afterShowEventFunc.push(function () {
                            self.isModalIsActivate = false;
                            $("#btnYes").on('click', function () {
                                ShowLoading();
                                $('#validationMessage').empty();
                                self.processSaveScoringParameter('SaveScoringParameter');
                            });
                            $("#btnNo").on('click', function () {
                                $('#surveyfrm').modal('toggle');
                            });
                        });
                        closeModalFunc.push(function () {
                            self.isModalIsActivate = true;
                            if (self.isSuccessyn) {
                                self.isSuccessyn = false;
                                var deferredAdd = $.Deferred();

                                $.when(deferredAdd).done(function () {
                                    location.reload();
                                });
                                Alert("Parameter have been successfully added", deferredAdd, "Information");
                            }
                        });

                        $(this).modalTask({
                            id: "surveyfrm",
                            title: "Add Parameter",
                            dataHtml: partialviewhtmltx ,
                            eventsAfterShow: afterShowEventFunc,
                            eventsCloseModal: closeModalFunc
                        });
                    });
                    self.populateDataInScoringParameterModal(self.pageUrl + '/AddEditScoringParameter/0', deferred);
                }
                event.preventDefault();
            });

            self.$scoringParamRow.each(function (index, obj) {
                $(this).find("td:first").on('click', function (event) {
                    var value = $(this).text();
                    self.editQuestionnaireData(value);
                });
   
            });

            self.$paramStatus.on('change', function () {
                self.$scoringParamTBody.empty();
                self.$dataTableScoringParam.destroy();
                self.filterScoringParambyStatus();
            });
        },

        processSaveScoringParameterMultiplierPerBU: function () {
            var self = this;
            $.ajax({
                url: self.pageUrl + "/" + method,
                type: "POST",
                data: $("#frmScoringParameter").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.successYn) {
                        self.isSuccessyn = true;
                        $('#surveyfrm').modal('toggle');
                    }
                    else {
                        $('#validationMessage').append(self.errorMsg(data.msgs));
                        //$("#validationMessage").append(data.Data);
                        //$("#validationMessage").addClass("text-danger");
                        HideLoading();
                    }
                    HideLoading();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            })
        },
        initializeTabEvents: function (module) {
            var self = this;

            //initialization

            self.$scoringParameterTab = $("#scoringParameterTab");
            self.$scoringParameterMultiplerTab = $("#scoringParameterMultiplerTab");


            $.each($("#buttonTabs .tablinks"), function () {
                $(this).removeClass("active");
                $(this).removeClass("selected");
            });
            $.each($("div.tabcontent"), function () {
                $(this).css("display", "none");
            });

            $("#" + module + "ButtonTab").addClass("active");
            $("#" + module + "ButtonTab").addClass("selected");
            $("#" + module + "Tab").show();

            switch (module) {
                case "scroingParameter":
                    self.$scoringParameterTab.show();
                    self.$scoringParameterMultiplerTab.hide();
                    break;
                case "scoringParameterDetails":
                    self.$scoringParameterTab.hide();
                    self.$scoringParameterMultiplerTab.hide();
                    break;
                case "scoringParameterMultipler":
                    self.$scoringParameterTab.hide();
                    self.$scoringParameterMultiplerTab.show();
                    break;
                default: break;
            }
        },
        processSaveScoringParameterMultiplier: function () {

            var self = this;
            $.ajax({
                url: self.pageUrl + "/SaveScoringParameterMultiplierBU",
                type: "POST",
                data: {
                    scoringParameterID: $("#ScoringParameterId").val(),
                    buID: $("#paramBU").val(),
                    fqID: $("#paramfiscalquarter").val(),
                    fyID: $("#paramfiscalyear").val(),
                    multiplier: $("#paramMultiplier").val()
                },
                dataType: 'json',
                success: function (data) {
                    if (data.successYn) {
                        self.isSuccessyn = true;
                        $('#surveyfrm').modal('toggle');
                    }
                    else {
                        $('#validationMessageScoringParameterMultiplier').append(self.errorMsg(data.msgs));
                        HideLoading();
                    }
                    HideLoading();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },

        processSaveScoringParameter: function (method) {
            console.log($("#frmScoringParameter").serialize());
            var self = this;
            $.ajax({
                url: self.pageUrl + "/"+ method,
                type: "POST",
                data: $("#frmScoringParameter").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.successYn) {
                        self.isSuccessyn = true;
                        $('#surveyfrm').modal('toggle');
                    }
                    else {
                        $('#validationMessageScoringParameter').append(self.errorMsg(data.msgs));
                        //$("#validationMessage").append(data.Data);
                        //$("#validationMessage").addClass("text-danger");
                        HideLoading();
                        }
                     HideLoading();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        errorMsg: function(errorList){
            errorString = "";
            $.each(errorList, function (index, obj) {
                errorString += "<li style='text-align:start'>" + obj + "</li>";
            });
            return errorString;
        },
        populateDataInScoringParameterModal: function (url, deferred) {
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    deferred.resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },

        filterScoringParambyStatus: function () {
            var self = this;
            $.ajax({
                url: self.pageUrl + '/Index',
                data:{
                    status: self.$paramStatus.val()
                },
                type: "POST",
                success: function (data) {
                    
                    self.$dataTableScoringParam = self.$scoringParam.DataTable();
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        }
    };


    var InitializedScoringParameterObjTask = function () {
        var scoringParameterObjTask = Object.create(ScoringParameterObjTask);
        scoringParameterObjTask.init();
    };

    InitializedScoringParameterObjTask();
});