﻿$(document).ready(function () {

    var SurveyRequestObjTask = {

        init: function () {
            var self = this;

            ShowLoading();
            self.declaration();
            self.setEvent();
            self.initializedSelectized();
        },
        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            //Viewing
            self.stakeholderSelectized;
            //Maintenance
            self.$ulStakeholder = $('#ulStockHolder');
            self.$ulSupplier = $('#ulSupplier');
            self.$btnSendEmail = $('#btnSendEmail');

            // VARIABLES
            self.selectedStakeholderID = [];
            self.selectedStakeholderName = [];
            self.stakeHoldersSupplier = [];
            self.isModalIsActivated = true;
        },
        setEvent: function () {
            var self = this;
         
            $(self.$btnSendEmail).on("click", function () {
                if (self.selectedStakeholderID.length > 0) {
                    self.showDeadlineModal();
                 
                }
                 else {
                    //alert("Please select stakeholder");
                    $(this).modalTask({
                        title: "Validation",
                        dataText: "<h4 style='text-align:center'>Please select Stakeholder</h4>",
                        btnYesHideyn: true,
                        btnNoHideyn: true
                    });
                }
            });
         

            $("ul.checked-list-box > li.list-group-item").on("click", function () {
                var $element = $(this),
                    $checkbox = $element.find("input[type=checkbox]");

                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.trigger("change");
            });

            $("li.list-group-item > input[type=checkbox]").on("change", function () {
                var $element = $(this),
                    $liParent = $element.parents("li:first"),
                    isChecked = $element.is(":checked");
                $liParent.find("i:first").remove();
                if (isChecked) {
                    $liParent.prepend("<i class='far fa-check-square' aria-hidden='true'></i>");
                }
                else {
                    $liParent.prepend("<i class='far fa-square' aria-hidden='true'></i>");
                }

                self.changeListColor($liParent, isChecked);

            });
       
            self.$ulStakeholder.on("click", function () {
                ShowLoading();
                self.selectedStakeholderID.length = 0;
                self.selectedStakeholderName.length = 0;
                self.stakeHoldersSupplier.length = 0;
                var $element = $(this);
                $.each($element.find("li.list-group-item.active"), function (index, content) {
                    self.selectedStakeholderID.push(parseInt($(this).data("employee-id")));
                    self.selectedStakeholderName.push($(this).text());
                });
                if (self.selectedStakeholderID.length > 0) {
                    self.getSupplier();
                }
                else {
                    self.displayStakeHoldersSupplier(0);
                    HideLoading();
                }

            });       
        },
        setEventsOnUpdaingEmailLetter: function () {
            var self = this;

            var deferred = $.Deferred();
            if (self.isModalIsActivated) {
                $.when(deferred).done(function (partialviewhtmltx) {
                    var afterShowEventFunc = [];
                    var closeModalFunc = [];
                    afterShowEventFunc.push(function () {
                        self.isModalIsActivate = false;
                        $("#btnYes").on('click', function () {
                            ShowLoading();
                            self.updateEmailMessage();
                           
                        });
                        $("#btnNo").on('click', function () {
                            $('#modal').modal('toggle');
                        });
                    });
                    closeModalFunc.push(function () {
                        self.isModalIsActivate = true;
                    });

                    $(this).modalTask({
                        title: "Email Letter",
                        dataHtml: partialviewhtmltx,
                        eventsAfterShow: afterShowEventFunc,
                        eventsCloseModal: closeModalFunc,
                        btnYesIconClass: "fa fa-send",
                        btnYestx: " Send",
                    });
                });
               
            }
            self.populateDataInEmailLetterModal(self.pageUrl + '/viewEmailMessage', deferred);
        },
        showDeadlineModal: function () {

            var self = this;
            var htmx = '';

            htmx += '<div class="row form-group centered-content">';
            //htmx +=   '<div class="col-xs-12 col-sm-6 col-md2 col-lg-2">';
            htmx +=   '<div style="text-align:center;padding-top:20px">';
            htmx += '<input type="text" id="deadline" class="date-picker"  readonly="readonly" style="text-align:center"/>';
            htmx +=   '</div>';
            htmx += '</div>';
            htmx += '<div class="row form-group centered-content">';
            htmx +=  '<div id="validationContainer1">';
            htmx += '<ul id="validationMessage" class="margin-zero text-center color-white" style="color: red; background-color: #fff "></ul>';
            htmx += '</div>';
            htmx += '</div>';

            if (self.isModalIsActivated) {
                    var afterShowEventFunc = [];
                    var closeModalFunc = [];
                    afterShowEventFunc.push(function () {
                        self.isModalIsActivate = false;
                        $('#deadline').datepicker({
                            dateFormat: "yy-mm-dd",
                            minDate: 'today'
                        });
                        $('#deadline').datepicker('setDate', 'today');
                        $("#btnYes").on('click', function () {
                            var objToday = new Date();
                            var myDate = new Date($('#deadline').val());
                            if ($('#deadline').val() === '') {
                                $('#validationMessage').empty();
                                $('#validationMessage').append("Please fill out the deadline field");
                                
                            }
                            else {
                                self.processDeadLine();
                                //alert('Please fill up the deadline field'); 
                            }
                        });
                        $("#btnNo").on('click', function () {
                            $('#modal').modal('toggle');
                        });
                    });
                    closeModalFunc.push(function () {
                        self.isModalIsActivate = true;
                    });

                    $(this).modalTask({
                        title: "Set Deadline",
                        dataHtml: htmx,
                        eventsAfterShow: afterShowEventFunc,
                        eventsCloseModal: closeModalFunc
                    });
            }
        },
        populateDataInEmailLetterModal: function (url, deferred) {
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    deferred.resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },
        sendEmailforSelectedStakeHolder: function () {
            var self = this;
            $.ajax({
                url: self.pageUrl + "/sendEmailStakeholdersSupplier",
                type: "POST",
                dataType: 'json',
                data: {
                    listOfStakeHolder: JSON.stringify(self.selectedStakeholderID),
                    listStakeholdersSupplier: JSON.stringify(self.stakeHoldersSupplier)
                },
                success: function (returnData) {
                    if (returnData.status) {
                        //alert("Send");
                        var deferredSend = $.Deferred();

                        $.when(deferredSend).done(function () {
                            location.reload();
                        });
                        Alert("Evaluation request successfully sent!", deferredSend, "Email Notification");

                    }
                    else {
                        Alert("Not Send");
                    }
                    HideLoading();
                },
                error: function () {
                    //alert("Not Send");
                    $(this).modalTask({
                        title: "",
                        dataHtml: "<style>tfooter{display:none}</style><h4 style='text-align:center'>Not Sent</h4>",
                        btnYesHideyn: true,
                        btnNoHideyn: true
                    });
                    HideLoading();
                }
            });
        },
        displayStakeHoldersSupplier: function (supplierbyStakeHolderList) {
            var self = this;

            if (supplierbyStakeHolderList !== 0) {
                var htmx = "";
                var stakeHolderSupplierList = supplierbyStakeHolderList.listOfStakeholdersSupplier;
                self.selectedStakeholderName.sort();
                for (var stakeholderCount = 0; stakeholderCount < self.selectedStakeholderID.length; stakeholderCount++) {
                    var supplierhtmtxt = "";
                    htmx += '<li>'
                    htmx += '<div class="font6">' + self.selectedStakeholderName[stakeholderCount] + '</div>';
                    htmx += '<div class="font2">';
                    for (var stakeholdersupplierCount = 0; stakeholdersupplierCount < stakeHolderSupplierList.length; stakeholdersupplierCount++) {
                        if (stakeHolderSupplierList[stakeholdersupplierCount].FullName.trim() == self.selectedStakeholderName[stakeholderCount].trim().substr(0, stakeHolderSupplierList[stakeholdersupplierCount].FullName.trim().length)) {
                            supplierhtmtxt += stakeHolderSupplierList[stakeholdersupplierCount].SupplierName + ", ";
                        }
                    }
                    htmx += '<span>' + supplierhtmtxt.substr(0, supplierhtmtxt.length - 2) + '</span>';
    
                    htmx += '</div>' + '</li>';
                }
                self.$ulSupplier = $('#ulSupplier').append(htmx);
            }
        },
        getSupplier: function () {
            var self = this;
            $.ajax({
                url: self.pageUrl + "/getSupplier",
                type: "GET",
                contentType: 'application/json',
                dataType:'json',
                data: {
                    listOfStakeHolder: JSON.stringify(self.selectedStakeholderID)
                },
                success: function(returnData){
                    self.displayStakeHoldersSupplier(returnData);
                    self.stakeHoldersSupplier = returnData.listOfStakeholdersSupplier;
                    HideLoading();
                },
                error: function(){
                    HideLoading();
                }
            });
        },
        updateEmailMessage: function () {
            var self = this;

            $.ajax({
                url: self.pageUrl + "/updateEmailMessage",
                type: "POST",
                data: $("#frmEmailLetter").serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.successYn) {
                        $('#modal').modal('toggle');
                        self.sendEmailforSelectedStakeHolder();
                    }
                    else {
                        $('#validationMessage').empty();
                        $('#validationMessage').append(self.errorMsg(data.msgs));
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        errorMsg: function (errorList) {
            errorString = "";
            $.each(errorList, function (index, obj) {
                errorString += '<li style="text-align:start">' + obj + "</li>";
            });
            return errorString;
        },
        getStockHolderSupplierData: function (deferred) {
            var self = this;

            $.ajax({
                url: self.pageUrl + '/getStakeHoldersSupplier',
                type: "GET",
                success: function (resultData) {
                    self.populateStockHolderList(resultData.listOfStickHolder);
                    deferred.resolve();
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        },
        populateStockHolderList: function(stockHolderList) {
            var self = this;

            var listOfStockHolder = "";
            for (var stockHolder = 0; stockHolder < stockHolderList.length; stockHolder++) {
                listOfStockHolder += '<li class="list-group-item" data-employee-id = ' + stockHolderList[stockHolder].EmployeeID + '>' + stockHolderList[stockHolder].FullName + '</li>';
            }
            self.$ulStockholder.append(listOfStockHolder);
        },
        getExcludedStakeholder: function(){
            $.each($(".selectize-dropdown-content").children($('.active')), function (index, obj) {
                console.log($(this).text());
            })
        },
        processDeadLine: function() {
            var self = this;

            $.ajax({
                url: self.pageUrl + "/setDeadline",
                type: "POST",
                data:{
                    listStakeholdersSupplier: JSON.stringify(self.stakeHoldersSupplier),
                    deadline: $('#deadline').val()
                }, 
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#modal').modal('toggle');
                        $('#modal').remove();
                        $('.modal-backdrop').remove();
                        self.setEventsOnUpdaingEmailLetter();
                    }
                    else {
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });

        },
        initializedSelectized: function () {
            var self = this;
            $.each($('#select-stakeholder').children("option:selected"), function (index, obj) {
                self.selectedStakeholderID.push($(this).val());
                self.selectedStakeholderName.push($(this).text());
            })
            if (self.selectedStakeholderID.length == 0) {
                HideLoading();
            }
            if (self.selectedStakeholderID.length > 0) {
                self.getSupplier();
            }

            self.stakeholderSelectized = $('#select-stakeholder').selectize({
                plugins: ['remove_button'],
                valueField: 'ID',
                labelField: 'Name',
                searchField: 'Name',
                options: [],
                sortField: [
                   {
                       field: 'Name',
                       direction: 'asc'
                   }
                ],
                create: false,
                onChange: function (value) {
                    self.$ulSupplier.empty();
                    self.selectedStakeholderID.length = 0;
                    self.selectedStakeholderName.length = 0;
                    if (value !== "") {
                        for (var valueCount = 0; valueCount < value.length; valueCount++) {
     
                            self.selectedStakeholderID.push(parseInt(value[valueCount]));
                            self.selectedStakeholderName.push(this.getItem(value[valueCount])[0].innerHTML);
                        }
                       
                    }
    
                    if (self.selectedStakeholderID.length > 0) {
                        ShowLoading();
                        self.getSupplier();
                    }
                },
                onItemRemove: function (value,item) {
                    $("#excludedStakeholder").append('<li id="' + 'Stakeholder' + value + '"><i class ="fa fa-user">' + item.text().substr(0, item.text().length - 1) + '</i> &nbsp;</li>');
                },
                onItemAdd: function(value, item){
                    $("#Stakeholder" + value).remove();
                },
                render:{
                    option: function (item, escape) {
                        return '<div>' + escape(item.Name) + '</div>';
                    }
                }
            })[0].selectize;
        }


    };
    var InitializedSurveyRequestObjTask = function () {
        var surveyRequestObjTask = Object.create(SurveyRequestObjTask);
        surveyRequestObjTask.init();
    };

    InitializedSurveyRequestObjTask();


});