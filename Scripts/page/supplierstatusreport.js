﻿//[20210528 JC]
$(document).ready(function () {
    var paymentFyID = 0;
    var paymentFqID = 0;
    var salesVolFyID = 0;
    var salesVolFqID = 0;
    var point1FyID = 0;
    var point1FqID = 0;
    var point2FyID = 0;
    var point2FqID = 0;
    var point3FyID = 0;
    var point3FqID = 0;
    var point4FyID = 0;
    var point4FqID = 0;
    var scoreFyID = 0;
    var scoreFqID = 0;

    var SupplierStatusObjTask = {

        init: function () {
            var self = this;
            var deferred = $.Deferred();
            ShowLoading();
            self.declaration(deferred);
            self.setEvent();
            $.when(deferred).done(function () {
                self.setDataTableEvents();
            });
        },
        declaration: function (deferred) {
            var self = this;
            //Maintenance
            self.$SupplierStatusTable = $('#tblSupplierStatus');
            self.$cbFilters = $("#cbFilters");
            self.$tRowFYFilters = $("#tRowFYFilters");
            self.$tRowFQFilters = $("#tRowFQFilters");
            self.$btnDownloadSupplierStatusReport = $("#btnDownloadSupplierStatusReport");
            self.$dataTableSupplierStatus = $("#dataTableSupplierStatus");
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.filters = $("select");

            $("select[id$=FY]").val(currentYear).change();
            $("select[id$=FQ]").val(currentQuarter).change();

            switch (currentQuarter) {
                case "2":
                    $("#ddlPoint1FQ").val("3").change();
                    $("#ddlPoint2FQ").val("4").change();
                    $("#ddlPoint3FQ").val("1").change();
                    $("#ddlPoint4FQ").val("2").change();
                    $("#ddlPoint1FY").val(currentYear - 1).change();
                    $("#ddlPoint2FY").val(currentYear - 1).change();
                    $("#ddlPoint3FY").val(currentYear).change();
                    $("#ddlPoint4FY").val(currentYear).change();
                    break;
                case "1":
                    $("#ddlPoint1FQ").val("2").change();
                    $("#ddlPoint2FQ").val("3").change();
                    $("#ddlPoint3FQ").val("4").change();
                    $("#ddlPoint4FQ").val("1").change();
                    $("#ddlPoint1FY").val(currentYear - 1).change();
                    $("#ddlPoint2FY").val(currentYear - 1).change();
                    $("#ddlPoint3FY").val(currentYear - 1).change();
                    $("#ddlPoint4FY").val(currentYear).change();
                    break;
                case "3":
                    $("#ddlPoint1FQ").val("4").change();
                    $("#ddlPoint2FQ").val("1").change();
                    $("#ddlPoint3FQ").val("2").change();
                    $("#ddlPoint4FQ").val("3").change();
                    $("#ddlPoint1FY").val(currentYear - 1).change();
                    $("#ddlPoint2FY").val(currentYear).change();
                    $("#ddlPoint3FY").val(currentYear).change();
                    $("#ddlPoint4FY").val(currentYear).change();
                    break;
                case "4":
                    $("#ddlPoint1FQ").val("1").change();
                    $("#ddlPoint2FQ").val("2").change();
                    $("#ddlPoint3FQ").val("3").change();
                    $("#ddlPoint4FQ").val("4").change();
                    break;
            }

            point1FyID = $("#ddlPoint1FY").val();
            point1FqID = $("#ddlPoint1FQ").val();
            point2FyID = $("#ddlPoint2FY").val();
            point2FqID = $("#ddlPoint2FQ").val();
            point3FyID = $("#ddlPoint3FY").val();
            point3FqID = $("#ddlPoint3FQ").val();
            point4FyID = $("#ddlPoint4FY").val();
            point4FqID = $("#ddlPoint4FQ").val();

            //$("#ddlPoint1FQ").val("1").change();
            //$("#ddlPoint2FQ").val("2").change();
            //$("#ddlPoint3FQ").val("3").change();
            //$("#ddlPoint4FQ").val("4").change();

            deferred.resolve();
            self.$tRowFYFilters.hide();
            self.$tRowFQFilters.hide();
        },
        setEvent: function () {
            var self = this;

            self.$dataTableSupplierStatus = self.$SupplierStatusTable.dataTable({
                "order": [[3, "desc"]],
                "initComplete": function (settings, json) {
                    HideLoading();
                }
            });
            self.$cbFilters.on('change', function (e) {

                if ($("#cbFilters").is(":checked")) {
                    self.$tRowFYFilters.show();
                    self.$tRowFQFilters.show();
                }
                else {
                    self.$tRowFYFilters.hide();
                    self.$tRowFQFilters.hide();
                }


            });

            function jsEscape(str) {
                return String(str).replace(/[^\w. ]/gi, function (c) {
                    return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4);
                });

            }

            self.$btnDownloadSupplierStatusReport.on('click', function (e) {
                //e.preventDefault();

                var deferred = $.Deferred();

                Confirmation("Are you sure to generate this list?", deferred);

                $.when(deferred).done(function (answer) {
                    if (answer == "yes")

                        //$.ajax({
                        //    type: "GET",
                        //    url: GetAppPath() + $("div[data-maincontainer-yn]").data("page-url") + "/ExportSupplierStatusReport",
                        //    data: {
                        //        paymentFyID: paymentFyID,
                        //        paymentFqID: paymentFqID,
                        //        salesVolFyID: salesVolFyID,
                        //        salesVolFqID: salesVolFqID,
                        //        point1FyID: point1FyID,
                        //        point1FqID: point1FqID,
                        //        point2FyID: point2FyID,
                        //        point2FqID: point2FqID,
                        //        point3FyID: point3FyID,
                        //        point3FqID: point3FqID,
                        //        point4FyID: point4FyID,
                        //        point4FqID: point4FqID,
                        //        scoreFyID: scoreFyID,
                        //        scoreFqID: scoreFqID,
                        //    },
                        //    success: function (data) {
                        //        var blob = new Blob([data]);
                        //        var link = document.createElement('a');
                        //        link.href = window.URL.createObjectURL(blob);
                        //        link.download = "SupplierStatusReport.xlsx";
                        //        link.click();
                        //    }
                        //})
                        window.open(self.pageUrl + "/ExportSupplierStatusReport?"+''+"paymentFyID=" + paymentFyID + "&paymentFqID=" + paymentFqID + "&salesVolFyID=" + salesVolFyID + "&salesVolFqID=" + salesVolFqID + "&point1FyID=" + point1FyID + "&point1FqID=" + point1FqID + "&point2FyID=" + point2FyID + "&point2FqID=" + point2FqID + "&point3FyID=" + point3FyID + "&point3FqID=" + point3FqID + "&point4FyID=" + point4FyID + "&point4FqID=" + point4FqID + "&scoreFyID=" + scoreFyID + "&scoreFqID=" + scoreFqID, "_blank");
                    //window.open(self.pageUrl + "/ExportSupplierStatusReport?" + window.location.search, "_blank");
                });
            });
            self.filters.on('change', function (e) {
                var currentFilter = this.id;

                if (currentFilter == "ddlPoint1FY" || currentFilter == "ddlPoint1FQ") {
                    $("#thQuarter4").empty();
                    $("#thQuarter4").text($("#ddlPoint1FY option:selected").text() + " " + $("#ddlPoint1FQ option:selected").text());
                }

                if (currentFilter == "ddlPoint2FY" || currentFilter == "ddlPoint2FQ") {
                    $("#thQuarter3").empty();
                    $("#thQuarter3").text($("#ddlPoint2FY option:selected").text() + " " + $("#ddlPoint2FQ option:selected").text());
                }

                if (currentFilter == "ddlPoint3FY" || currentFilter == "ddlPoint3FQ") {
                    $("#thQuarter2").empty();
                    $("#thQuarter2").text($("#ddlPoint3FY option:selected").text() + " " + $("#ddlPoint3FQ option:selected").text());
                }

                if (currentFilter == "ddlPoint4FY" || currentFilter == "ddlPoint4FQ") {
                    $("#thQuarter1").empty();
                    $("#thQuarter1").text($("#ddlPoint4FY option:selected").text() + " " + $("#ddlPoint4FQ option:selected").text());
                }

                switch (currentFilter) {
                    case "ddlPaymentTermsFY":
                        paymentFyID = $("#" + currentFilter).val();
                        break;
                    case "ddlSpendInVolFY":
                        salesVolFyID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint1FY":
                        point1FyID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint2FY":
                        point2FyID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint3FY":
                        point3FyID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint4FY":
                        point4FyID = $("#" + currentFilter).val();
                        break;
                    case "ddlScoreFY":
                        scoreFyID = $("#" + currentFilter).val();
                        break;
                    case "ddlPaymentTermsFQ":
                        paymentFqID = $("#" + currentFilter).val();
                        break;
                    case "ddlSpendInVolFQ":
                        salesVolFqID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint1FQ":
                        point1FqID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint2FQ":
                        point2FqID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint3FQ":
                        point3FqID = $("#" + currentFilter).val();
                        break;
                    case "ddlPoint4FQ":
                        point4FqID = $("#" + currentFilter).val();
                        break;
                    case "ddlScoreFQ":
                        scoreFqID = $("#" + currentFilter).val();
                        break;
                }
                self.setDataTableEvents();
            });

        },
        setDataTableEvents: function () {
            var self = this;
            $("#tblSupplierStatus").DataTable().destroy();

            $("#tblSupplierStatus").DataTable({
                "order": [],
                "searchDelay": 100,
                "bServerSide": true,
                "ajax": {
                    "url": GetAppPath() + $("div[data-maincontainer-yn]").data("page-url") + "/SupplierStatusAjaxList",
                    "data": {
                        paymentFyID: paymentFyID,
                        paymentFqID: paymentFqID,
                        salesVolFyID: salesVolFyID,
                        salesVolFqID: salesVolFqID,
                        point1FyID: point1FyID,
                        point1FqID: point1FqID,
                        point2FyID: point2FyID,
                        point2FqID: point2FqID,
                        point3FyID: point3FyID,
                        point3FqID: point3FqID,
                        point4FyID: point4FyID,
                        point4FqID: point4FqID,
                        scoreFyID: scoreFyID,
                        scoreFqID: scoreFqID,
                    },
                    "type": "POST"
                },
                "sPaginationType": "simple_numbers",
                "bFilter": false,
                "bInfo": false,
                "iDisplayLength": 10,
                "oLanguage": {
                    "sLengthMenu": " _MENU_ ",
                    "sInfo": "_START_ to _END_ of _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sInfoFiltered": ""
                },
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "bSortable": false,
                        "bSearchable": false,
                    },
                    {
                        "aTargets": [1],
                        "bSortable": false,
                        "bSearchable": false
                    },
                    {
                        "aTargets": [2],
                        "bSortable": false,
                        "bSearchable": false,
                    },
                    {
                        "aTargets": [3],
                        "bSortable": false,
                        "bSearchable": false
                    },
                    {
                        "aTargets": [4],
                        "bSortable": false,
                        "bSearchable": false
                    },
                    {
                        "aTargets": [5],
                        "bSortable": false,
                        "bSearchable": false
                    },
                    {
                        "aTargets": [6],
                        "bSortable": false,
                        "bSearchable": false
                    },
                    {
                        "aTargets": [7],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [8],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [9],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [10],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [11],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [12],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [13],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [14],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                    {
                        "aTargets": [15],
                        "bSortable": false,
                        "bSearchable": false,
                        "className": 'dt-body-center'
                    },
                ],
                "preDrawCallback": function () {
                    ShowLoading();
                },
                "drawCallback": function () {
                    HideLoading();
                    $.each($("#supplierTable tbody").find('.a-edit'), function (i, obj) {
                        $(obj).off('click').on('click', function (e) {
                            self.$isModalOpen = !self.$isModalOpen;
                            e.preventDefault();
                            ShowLoading();
                            var dataID = $(this).data('id');
                            var deferredPartialViewObjInit = $.Deferred();
                            self.editSupplierData(dataID, "edit", deferredPartialViewObjInit, $(this).data('buid'));
                            $.when(deferredPartialViewObjInit).done(function () {
                                initPartialViewObj();
                                self.initializeDatepicker();
                                HideLoading();
                            });
                        })
                    });

                }
            });
        },
    }


    var InitializedSupplierStatusObjTask = function () {
        var supplierStatusObjTask = Object.create(SupplierStatusObjTask);
        supplierStatusObjTask.init();
    };

    InitializedSupplierStatusObjTask();

});