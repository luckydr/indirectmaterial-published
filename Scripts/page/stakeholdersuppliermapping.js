﻿$(function () {
    var HomeTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();            
        },

        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.taskcd = self.$container.data("task-cd") == undefined ? "view" : self.$container.data("task-cd");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$ulSuppliers = self.$container.find("ul#ulSuppliers");
            self.$ulStakeHolders = self.$container.find("ul#ulStakeHolders");
            self.stakeholderId = 0;
            self.fyid = parseInt($("#optFYID").val());
            self.fqid = parseInt($("#optFQID").val());
            self.assignedYn = true;

            if (self.taskcd == "view") {
                self.$selectFiscalYearID = self.$container.find("select#fiscalYearID");
                self.$selectFiscalQuarterID = self.$container.find("select#fiscalQuarterID");
                self.$btnAdd = $("#btnAddStakeholderSupplierMapping");
            } else {                
                self.$btnSave = $("#btnSave");
                self.counter = 0;
                self.$optSupplierStatus = self.$container.find("#optSupplierStatus");
                self.$optSuppplierType = self.$container.find("#optSuppplierType");
                self.$optFQID = $("#optFQID");
                self.$optFYID = $("#optFYID");
                self.$txtSearchName = self.$container.find("#txtSearchName");
                self.$txtSearchSupplier = self.$container.find("#txtSearchSupplier");
            }


            // FOR VIEW MODE
            //self.$tblShsm = $("#tblShsm");
            
            //self.$ulStakeHolders = $("#ulStakeHolders");

            // FOR MAINTENANCE
            //self.$ulStakeHolders = $("#ulStakeHolders");
            //self.$ulSupplierStatus = $("#ulSupplierStatus");
            //self.$btnSupplierStatus = $("#btnSupplierStatus");
            //self.$ulSupplier = $("#ulSupplier");

            // VARIABLES
            //self.saveObj = undefined;
            //self.selectedStakeholder = 0;
            //self.maintenanceSelectedStakeholderID = 0;
            //self.counter = 0;

            // Initialize flow control
        },
        changeListColor: function ($element, isChecked) {
            var self = this;

            if (isChecked) {
                $element.addClass("list-group-item-primary active");
            } else {
                $element.removeClass("list-group-item-primary active");
            }
        },

        setEvents: function () {
            var self = this;

            if (self.taskcd == "view") {
                self.$btnAdd.on("click", function (e) {
                    window.location = "/StakeholderSupplierMapping/Maintenance";
                });
                
                $("select#fiscalYearID, select#fiscalQuarterID").on("change", function () {
                    var $element = $(this);
                    self.fyid = parseInt(self.$container.find("select#fiscalYearID").val());
                    self.fqid = parseInt(self.$container.find("select#fiscalQuarterID").val());                    
                    $.get(self.pageUrl + "/GetStakeholders", { fyid: self.fyid, fqid: self.fqid }, function (returnData) {
                        var data = returnData.Data;
                        var htmltx = "";
                        var supplierHtmlTx = "";
                        self.$ulStakeHolders.empty();

                        $.each(data, function (ind, obj) {
                            htmltx += "<li class='list-group-item' data-employee-id='" + obj.EmployeeID + "'>" + obj.FullName + "</li>";
                        });

                        if (htmltx.length == 0) {
                            htmltx = "<li class='list-group-item' style='cursor:not-allowed'>No Available Stakeholder</li>";
                        }

                        self.$ulStakeHolders.append(htmltx);
                        self.setListEvents();
                        self.$ulSuppliers.empty();
                        supplierHtmlTx = data.length > 0 ? "<li class='list-group-item' style='cursor:not-allowed'>Please select Stakeholder</li>" : "<li class='list-group-item' style='cursor:not-allowed'>Stakeholder has no assigned supplier for the selected Fiscal year and quarter</li>";
                        self.$ulSuppliers.append(supplierHtmlTx);
                        HideLoading();
                    }).fail(function () {
                        Alert("Error");
                        HideLoading();
                    });
                });

                self.setListEvents();

            } else if (self.taskcd == "add") {
                // STAKEHOLDER LIST GROUP

                $("ul:not(.checked-list-box) > li.list-group-item").on("click", function () {

                    var $element = $(this),
                        isChecked = !$element.hasClass("active");

                    if (!isChecked) return false;
                    

                    //self.maintenanceSelectedStakeholderID = $element.data("employee-id");
                    self.$ulStakeHolders.find("li.active").removeClass("list-group-item-primary active");
                    self.stakeholderId = $element.data("employee-id");
                    self.getSuppliers();
                    self.$optSupplierStatus.prop("disabled", false);

                    //if (isChecked) {
                    //    self.$ulStakeHolders.find("li.active").removeClass("list-group-item-primary active");
                    //    self.selectedStakeholder = parseInt($element.data("employee-id"));
                    //    self.$optSupplierStatus.prop("disabled", false);
                    //    self.$btnSave.hide();
                    //    self.getSuppliers();
                    //} else {
                    //    self.selectedStakeholder = 0;
                    //    self.$optSupplierStatus.prop("disabled", true);
                    //    self.displaySuppliers();
                    //}
                    self.changeListColor($element, isChecked);
                });

                self.$btnSave.on("click", function (e) {                    
                    var recordids = [];
                    var tempSupplierIds = undefined;                    

                    tempSupplierIds = self.$ulSuppliers.find("li.active");
                    $.each(tempSupplierIds, function (ind, obj) {                        
                        recordids.push(parseInt($(obj).data("supplier-id")));
                    });                    

                    self.saveObj = { employeeId: self.stakeholderId, supplierIds: recordids, fQuarterID: self.fqid, fyid: self.fyid };
                    self.saveSSM();
                });

                // SUPPLIER STATUS DROPDOWNLIST
                self.$optSupplierStatus.on("change", function (e) {
                    var $element = $(this);

                    setTimeout(function () {
                        self.assignedYn = self.$optSupplierStatus.val() == "true" ? true : false;
                        if (self.assignedYn) {
                            self.$optSuppplierType.parents("div[data-suptype-parent]").show();
                            self.$btnSave.parents("div:first").hide();
                            self.$txtSearchSupplier.parents("div[data-searchsupplier-yn]").hide();
                        } else {
                            self.$optSuppplierType.parents("div[data-suptype-parent]").hide();
                            self.$txtSearchSupplier.parents("div[data-searchsupplier-yn]").show();
                        }

                        self.getSuppliers();
                    });
                });

                self.$optSuppplierType.on("change", function (e) {
                    var type = parseInt($(this).val());
                    self.$ulSuppliers.find("li.inactive").removeClass("inactive");
                    self.$ulSuppliers.find("li[data-noavailable-txt]").remove();
                    var ctr = 0;
                    if (type == 1) {
                        ctr = self.$ulSuppliers.find("li[data-assessed-yn=true]").length;
                        self.$ulSuppliers.find("li:not([data-assessed-yn=true])").addClass("inactive");
                    } else if (type == 2) {
                        ctr = self.$ulSuppliers.find("li:not([data-assessed-yn=true])").length;
                        self.$ulSuppliers.find("li[data-assessed-yn=true]").addClass("inactive");
                    } else {
                        ctr = self.$ulSuppliers.find("li").length;
                    }

                    if (ctr == 0) {
                        self.$ulSuppliers.append("<li class='list-group-item' data-noavailable-txt='true'>Stakeholder has no assigned supplier for the selected Fiscal year and quarter</li>");
                    }
                });

                self.$optFQID.on("change", function () {
                    self.fqid = parseInt($(this).val());
                    self.getSuppliers();
                    //if (self.assignedYn) {
                    //    self.getSuppliers();
                    //} else {

                    //}
                });

                self.$optFYID.on("change", function () {
                    self.fyid = parseInt($(this).val());
                    self.getSuppliers();
                });

                self.$txtSearchSupplier.on("keyup", function (e) {
                    var $element = $(e.currentTarget);
                    var value = $element.val();
                    self.$ulSuppliers.find("li").removeClass("inactive");
                    if (value.length >= 2) {
                        self.$ulSuppliers.find("li:not(:contains('" + value + "'))").addClass("inactive");
                    }
                });

                self.$txtSearchName.on("keyup", function (e) {
                    var $element = $(e.currentTarget);
                    var value = $element.val();
                    self.$ulStakeHolders.find("li").removeClass("inactive");
                    if (value.length >= 2) {
                        self.$ulStakeHolders.find("li:not(:contains('" + value + "'))").addClass("inactive");
                    }
                })

                //self.$txtSearchSupplier.autocomplete({
                //    source: function (request, response) {
                //        $.ajax({
                //            url: self.pageUrl + "/ACSuppliers",
                //            dataType: "JSON",
                //            data: {
                //                q: request.term
                //            },
                //            success: function (data) {
                //                response($.map(data.Data, function (item) {
                //                    return {
                //                        label: item.SupplierName,
                //                        value: item.SupplierName,
                //                        DataID: item.SupplierID
                //                    }
                //                }));
                //            },
                //            error: function (data) {
                //                var x = data;
                //            }
                //        });
                //    },
                //    select: function (event, ui) {
                //        //self.$txtSearchName.removeAttr("data-stakeholder-id").attr("data-stakeholder-id", ui.item.DataID);

                //        //self.$ulStakeHolders.animate({
                //        //    scrollTop: $('#ulStakeHolders li[data-employee-id="' + ui.item.DataID + '"]').position().top
                //        //}, 'slow');

                //        //self.$ulStakeHolders.find("li[data-employee-id='" + ui.item.DataID + "']").trigger("click");
                //    }
                //});

                //self.$txtSearchName.autocomplete({
                //    source: function (request, response) {
                //        $.ajax({
                //            url: self.pageUrl + "/ACStakeholders",
                //            dataType: "JSON",
                //            data: {
                //                q: request.term
                //            },
                //            success: function (data) {
                //                response($.map(data.Data, function (item) {
                //                    return {
                //                        label: item.FullName,
                //                        value: item.FullName,
                //                        DataID: item.EmployeeID
                //                    }
                //                }));
                //            },
                //            error: function (data) {
                //                var x = data;
                //            }
                //        });
                //    },
                //    select: function (event, ui) {
                //        self.$txtSearchName.removeAttr("data-stakeholder-id").attr("data-stakeholder-id", ui.item.DataID);
                        
                //        self.$ulStakeHolders.animate({
                //            scrollTop: $('#ulStakeHolders li[data-employee-id="' + ui.item.DataID + '"]').position().top
                //        }, 'slow');

                //        self.$ulStakeHolders.find("li[data-employee-id='" + ui.item.DataID + "']").trigger("click");
                //    }
                //});
            }

            $.expr[':'].contains = function (a, i, m) {
                return $(a).text().toUpperCase()
                    .indexOf(m[3].toUpperCase()) >= 0;
            };

            self.setSupplierListEvent();

            self.$ulStakeHolders.find('li.list-group-item').first().trigger('click');

            //$("#ulStakeHolders li.list-group-item").first().trigger('click');
        },

        setListEvents: function () {
            var self = this;

            // VIEWING STAKEHOLDER ON LI CLICKED
            self.$ulStakeHolders.find("li.list-group-item").on("click", function () {
                var element = $(this);
                
                if (element.hasClass('active') || element.data("employee-id") == null) return false;

                $.each($("ul#ulStakeHolders li.list-group-item"), function () {
                    $(this).removeClass("list-group-item-primary active");
                });
                self.stakeholderId = element.data("employee-id");

                var isCheck = !element.hasClass("active");

                if (isCheck) {
                    self.changeListColor(element, isCheck);

                    self.fyid = parseInt(self.$selectFiscalYearID.val()),
                    self.fqid = parseInt(self.$selectFiscalQuarterID.val());

                    ShowLoading();

                    self.getSuppliers();
                }
                else self.changeListColor(element, isCheck);
            });
        },

        getSuppliers: function (assignedYn) {
            var self = this;
            ShowLoading();
            //assignedYn = assignedYn == undefined ? true : assignedYn;

            $.ajax({
                url: self.pageUrl + "/GetSupplier",
                dataType: "JSON",
                type: "POST",
                data: JSON.stringify({ stakeholderId: self.stakeholderId, fyid: self.fyid, fqid: self.fqid, assignedYn: self.assignedYn, taskcd: self.taskcd }),
                contentType: 'application/json',
                success: function (returnData) {
                    var htmltx = "";
                    $.each(returnData.Data, function (ind, obj) {
                        if (self.taskcd == "add") {

                            var supplierid = 0, shsmid = 0, style = "", icon = "", isAssessed = false, btnDelete = "", dataAttributeAssesed = "",localVendorNumber = "";
                            if (self.assignedYn) {
                                isAssessed = obj.Assessed != null ? true : false;
                                style = !isAssessed ? style : "cursor:not-allowed;";
                                supplierid = obj.SupplierMaster.SupplierID;
                                suppliername = obj.SupplierMaster.SupplierName;
                                localVendorNumber = obj.SupplierMaster.LocalVendorNumber;
                                //supplierid = obj.SupplierID;
                                //suppliername = obj.SupplierName;
                                shsmid = obj.StakeholderSupplierID;
                                btnDelete = !isAssessed ? "<button type='button' class='pull-left fa fa-times' data-delete-supplier='true' id='btnDelete'></button>" : "<small class='pull-right' style='font-size:12px; color: #008000'>Assessment Done</small>";
                                dataAttributeAssesed = isAssessed ? "data-assessed-yn='true'" : "";
                            } else {
                                suppliername = obj.SupplierName;
                                supplierid = obj.SupplierID;
                                localVendorNumber = obj.LocalVendorNumber;
                                icon = "<i class='fa fa-square-o' aria-hidden='true'></i> ";
                            }

                            htmltx +=
                               "<li class='list-group-item' " + dataAttributeAssesed + " style='" + style + "'data-shsm-id='" + shsmid + "' data-supplier-id='" + supplierid + "' data-supplier-id='" + supplierid + "' data-assessed-yn='" + isAssessed + "'>" +
                            icon + localVendorNumber + "-" +
                               suppliername +
                               "   <input type='checkbox' class='hidden'>" +
                               btnDelete
                            "</li>";

                        }
                        else {
                            htmltx += "<li class='list-group-item' style=' data-supplier-id='" + obj.SupplierMaster.SupplierID + "'>" + obj.SupplierMaster.LocalVendorNumber +
                                        "-" + obj.SupplierMaster.SupplierName + "</li>";
                        }

                    });

                    if (htmltx.length == 0) {
                        htmltx += "<li class='list-group-item' data-noavailable-txt='true'>Stakeholder has no assigned supplier for the selected Fiscal year and quarter</li>";
                    }
                    self.$ulSuppliers.empty();
                    self.$ulSuppliers.append(htmltx);
                    if (self.taskcd == "add") self.setSupplierListEvent();
                    HideLoading();
                },
                error: function (data) {
                    Alert("Failed to get the data");
                    HideLoading();
                }
            });

            //$.get(self.pageUrl + "/GetSupplier", { stakeholderId: self.stakeholderId, fyid: self.fyid, fqid: self.fqid, assignedYn: self.assignedYn, taskcd: self.taskcd }, function (returnData) {
            //    var htmltx = "";
            //    $.each(returnData.Data, function (ind, obj) {
            //        if (self.taskcd == "add") {

            //            var supplierid = 0, shsmid = 0, style = "", icon = "", isAssessed = false, btnDelete = "", dataAttributeAssesed = "";
            //            if (self.assignedYn) {
            //                isAssessed = obj.Assessed != null ? true : false;
            //                style = !isAssessed ? style : "cursor:not-allowed;";
            //                supplierid = obj.SupplierMaster.SupplierID;
            //                suppliername = obj.SupplierMaster.SupplierName;
            //                shsmid = obj.StakeholderSupplierID;
            //                btnDelete = !isAssessed ? "<button type='button' class='pull-right fa fa-user-times' data-delete-supplier='true' id='btnDelete' style='color:#2d383f'></button>" : "<small class='pull-right' style='font-size:12px; color: #008000'>Already been assessed</small>";
            //                dataAttributeAssesed = isAssessed ? "data-assessed-yn='true'" : "";
            //            } else {
            //                suppliername = obj.SupplierName;
            //                supplierid = obj.SupplierID;
            //                icon = "<i class='fa fa-square-o' aria-hidden='true'></i> ";
            //            }

            //            htmltx +=
            //               "<li class='list-group-item' " + dataAttributeAssesed + " style='" + style + "'data-shsm-id='" + shsmid + "' data-supplier-id='" + supplierid + "' data-supplier-id='" + supplierid + "' data-assessed='" + isAssessed + "'>" +
            //               icon +
            //               suppliername +
            //               "   <input type='checkbox' class='hidden'>" +
            //               btnDelete
            //            "</li>";

            //        }
            //        else {
            //            htmltx += "<li class='list-group-item' style=' data-supplier-id='" + obj.SupplierMaster.SupplierID + "'>" +
            //                          obj.SupplierMaster.SupplierName + "</li>";
            //        }
                   
            //    });

            //    if (htmltx.length == 0) {
            //        htmltx += "<li class='list-group-item'>No Available Supplier</li>";
            //    }
            //    self.$ulSuppliers.empty();
            //    self.$ulSuppliers.append(htmltx);
            //    if (self.taskcd == "add") self.setSupplierListEvent();
            //    HideLoading();
            //}).fail(function (err) {
            //    alert("Failed to get the data");
            //    HideLoading();
            //});

            //stakeholderId
            //if (stakeholderId == undefined) stakeholderId = 0;

            //var dataObj = { assignedYn: self.assignedYn, stakeholderId: self.selectedStakeholder }

            //$.ajax({
            //    url: "/StakeholderSupplierMapping/GetSuppliers",
            //    data: JSON.stringify(dataObj),
            //    type: "POST",
            //    contentType: 'application/json; charset=utf-8',
            //    dataType: 'json',
            //    success: function (returnData) {
            //        self.displaySuppliers(returnData.data);
            //    },
            //    error: function (data) {
            //        alert("Something went wrong...");
            //    },
            //});
            //var fqid = self.$optFQID.val(),
            //        fyid = self.$optFYID.val(),
            //        params = self.assignedYn + '/' + self.maintenanceSelectedStakeholderID + '/' + fyid + '/' + fqid;

            //$.ajax({
            //    url: '/StakeholderSupplierMapping/GetSuppliers/' + params,
            //    type: 'GET',
            //    success: function (response) {
            //        self.displaySuppliers(response.data);
            //    }
            //});
        },

        setSupplierListEvent: function () {
            var self = this;

            // Stakeholder LIST GROUP 
            if (!self.assignedYn) {
                $("ul.checked-list-box > li.list-group-item").on("click", function () {
                    var $element = $(this),
                        $checkbox = $element.find("input[type=checkbox]");

                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.trigger("change");
                });

                $("li.list-group-item > input[type=checkbox]").on("change", function () {
                    var $element = $(this),
                        $liParent = $element.parents("li:first"),
                        isChecked = $element.is(":checked"),
                        isAssessed = $liParent.data("assessed");

                    $liParent.find("i:first").remove();
                    if (isChecked && !isAssessed) {
                        $liParent.prepend("<i class='fa fa-check-square-o' aria-hidden='true'></i>");
                        self.counter++;
                        self.changeListColor($liParent, isChecked);
                    }
                    if (!isChecked && !isAssessed) {
                        $liParent.prepend("<i class='fa fa-square-o' aria-hidden='true'></i>");
                        self.counter--;
                        self.changeListColor($liParent, isChecked);
                    }

                    if (self.counter > 0) self.$btnSave.parents("div:first").show();
                    else self.$btnSave.parents("div:first").hide();
                });
            } else if (self.assignedYn) {
                $("li.list-group-item > button[data-delete-supplier=true]").on("click", function () {
                    var recordid = parseInt($(this).parents("li:first").data("shsm-id"));
                    ShowLoading();
                    $.post(self.pageUrl + "/DeleteSSM", { ssmID: recordid }, function (returnData) {
                        var x = returnData;
                        if (returnData.successYn) {
                            self.$ulSuppliers.find("li[data-shsm-id='" + recordid + "']").remove();
                        } else {
                            Alert("Record cannot be deleted");
                        }
                        HideLoading();
                    }).fail(function (error) {
                        Alert("Failed to delete");
                    });
                })

            }
        },

        displaySuppliers: function (data) {
            var self = this,
                validYn = false,
                htmlTx = "";

            console.log(data)
            
            if ($.isArray(data)) {
                if (data.length > 0) {
                    validYn = true;
                    var state = "";
                    //var state = self.assignedYn != undefined && self.assignedYn ? "list-group-item-primary active" : "";
                    //var icon = self.assignedYn != undefined && self.assignedYn ? "<i class='fa fa-check-square-o' aria-hidden='true'></i>" : "<i class='fa fa-square-o' aria-hidden='true'></i>";
                    var deleteIcon = self.assignedYn ? "<i class='fa fa-trash-o' aria-hidden='true'></i>" : "";
                    $.each(data, function (ind, obj) {
                        var supplierid = "", suppliername = "", shsmid = "", btnDelete = "", isAssessed = "", icon = "", style = "cursor:pointer;";
                        if (self.assignedYn) {
                            supplierid = obj.SupplierMaster.SupplierID;
                            suppliername = obj.SupplierMaster.SupplierName;
                            shsmid = obj.StakeholderSupplierID;
                            btnDelete = obj.Assessed == null ? "<button type='button' class='pull-left fa fa-times' data-delete-supplier='true' id='btnDelete'" + shsmid + "'></button>" : "<small class='pull-right' style='font-size:12px; color: #008000'>Assessment Done</small>";
                            isAssessed = obj.Assessed != null ? true : false;
                            icon = obj.Assessed == null ? "<i class='fa fa-square-o' aria-hidden='true'></i> " : "";
                            style = obj.Assessed == null ? style : "cursor:not-allowed;";
                        } else {
                            supplierid = obj.SupplierID;
                            suppliername = obj.SupplierName;
                        }

                        htmlTx +=
                            "<li class='list-group-item' style='" + style + "' data-shsm-id='" + shsmid + "' data-supplier-id='" + supplierid + "' data-assessed-yn='"+ isAssessed +"'>" +
                           icon + obj.SupplierMaster.LocalVendorNumber + "-" +
                            suppliername +
                            "   <input type='checkbox' class='hidden'>" +
                            btnDelete
                            "</li>";
                    });
                }
            }

            self.$ulSupplier.empty();
            if (!validYn) {
                htmlTx = "<div class='alert alert-warning' role='alert'>No data to display</div >"
            }

            self.$ulSupplier.append(htmlTx);
            self.setSupplierListEvent();
        },

        searchByName: function () {
            var self = this;
        },

        // CRUD
        //deleteSSM: function () {
        //    var self = this;
        //    var dataObj = JSON.stringify(self.saveObj);
        //    ShowLoading();
        //    $.ajax({
        //        url: "/StakeholderSupplierMapping/DeleteSSM",
        //        data: dataObj,
        //        type: "POST",
        //        contentType: 'application/json; charset=utf-8',
        //        dataType: 'json',
        //        success: function (returnData) {
        //            var x = returnData;
        //            self.saveObj = undefined;
        //            location.reload();
        //        },
        //        error: function (data) {
        //            alert("Something went wrong...");
        //            self.saveObj = undefined;
        //            self.taskcd = "";
        //            HideLoading();
        //        },
        //    });
        //},

        saveSSM: function () {
            var self = this;

            var dataObj = JSON.stringify(self.saveObj);
            //var MethodName = self.taskcd == "add" ? "SaveSSM" : "UpdateSSM";
            ShowLoading();
            $.ajax({
                url: self.pageUrl + "/SaveSSM",
                data: dataObj,
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (returnData) {
                    if (returnData.successYn) {
                        self.$optSupplierStatus.val("true");
                        self.$optSupplierStatus.trigger("change");
                        self.saveObj = undefined;
                    } else {
                        Alert("Cannot save the record.");
                    }
                    //location.reload();
                },
                error: function (data) {
                    Alert("Something went wrong");
                    self.saveObj = undefined;
                    HideLoading();
                },
            });
        }

}

    $.fn.initHome = function () {
        var homeTask = Object.create(HomeTask);
        homeTask.init();
    }

    $("body").initHome();
    
})
