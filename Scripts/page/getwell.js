﻿$(document).ready(function () {
    var getWellTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.setDataTableEvents();
        },
        declaration: function () {
            var self = this;

            self.$chkGetWell = $("#chkGetWell");
            self.$getWellTable = $("#getWellTable");
            self.$getWellTableTbody = $("#getWellTbody");
            self.$getWellTableTr = $("#getWellTbody tr");
            self.$getWellObjectsToPost = [];
            self.$btnSaveGetWellProgram = $("#btnSaveGetWellProgram");
            self.$btnSaveGetWellProgramText = $("#btnSaveGetWellProgramText");

            self.$willClear = false;

            self.$roleId = $("#roleId").val();
            self.$activeFyid = $("#activeFyid").val();
            self.$activeFqid = $("#activeFqid").val();
            self.$btnDownloadGetWellProgram = $("#btnDownloadGetWellProgram");
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$supplierIDsGet = []
            self.$getSupplierCount = $("#gwpCount").val();

            $(':checkbox:checked').each(function (index) {
                var id = $(this).attr('data-supplierId');
                self.$supplierIDsGet.push(id);
                console.log(id);

                var isChecked = $(this).is(':checked'),
                    buid = $(this).data('buid'),
                    supplierid = id,
                    fyid = $(this).data('fyid'),
                    fqid = $(this).data('fqid'),
                    getwellid = $(this).data('getwellid'),
                    obj = { isChecked: isChecked, businessunitId: buid, supplierId: supplierid, fyId: fyid, fqId: fqid, getwellId: getwellid };

                self.$getWellObjectsToPost.push(obj)
            });
            console.log(self.$getWellObjectsToPost);

        },
        setEvents: function () {
            var self = this;

            $("select#fyid, select#fqid").on("change", function () {
                var fyid = $("select#fyid").val(),
                    fqid = $("select#fqid").val(),
                    obj = { fyid: fyid, fqid: fqid };

                ShowLoading();

                $.ajax({
                    type: "GET",
                    url: "GetWellProgram/Filter",
                    data: obj,
                    dataType: "json",
                    success: function (response) {
                        var deferred = $.Deferred();
                        self.rebuildGetWellTable(response, deferred);
                        self.changeTableHeader(response.points);
                        $.when(deferred).done(function () {
                            HideLoading();
                            self.setDataTableEvents();
                        })
                    }
                })
            });
       
            //$('#uncheckAll').click(function () {
            //    if ($(this).is(':checked')) {
            //        //self.$getWellObjectsToPost.push(obj)
            //        //self.$getWellObjectsToPost.splice(0, self.$getWellObjectsToPost.length)
            //        //console.log(self.$getWellObjectsToPost);

            //        self.$btnSaveGetWellProgram.show();

            //        self.$getWellTable
            //            .find('input[type="checkbox"]')
            //            .prop('checked', false);
            //    }
            //    else {
            //        //window.location.reload();
            //        InitializeGetWell();
            //    }
            //});

            $(':checkbox').click(function () {
                if ($(this).is(':checked')) {
                    $(this).attr('checked', true);
                    var id = $(this).attr('data-supplierId');
                    self.$supplierIDsGet.push(id);
                    console.log('here');
                }

                else {
                    $(this).attr('checked', false);
                    var id = $(this).attr('data-supplierId');
                    self.$supplierIDsGet = jQuery.grep(self.$supplierIDsGet, function (value) {
                        return value != id;
                    });
                }
            });

            self.$btnSaveGetWellProgram.on('click', function () {
                ShowLoading();


                if (self.$willClear) {

                    var emptyArray = self.$getWellObjectsToPost.splice(0, self.$getWellObjectsToPost.length);
                    //var obj = { isChecked: false, businessunitId: 0, supplierId: 0, fyId: self.$activeFyid, fqId: self.$activeFqid, getwellId: 0 };
                    //self.$getWellObjectsToPost.push(obj);

                    $.ajax({
                        type: "POST",
                        url: "GetWellProgram/Post",
                        data: JSON.stringify({
                            getWellObjectsToPost: emptyArray,
                            fyid: self.$activeFyid,
                            fqid: self.$activeFqid
                        }),
                        contentType: "application/json; charset=UTF-8",
                        success: function (response) {
                            if (response.successOperation) {
                                var refresh = setTimeout(function () {
                                    location.reload();
                                }, 1000);
                                Alert(undefined, refresh);
                            }
                        }
                    });
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "GetWellProgram/Post",
                        data: JSON.stringify({
                            getWellObjectsToPost: self.$getWellObjectsToPost,
                            fyid: self.$activeFyid,
                            fqid: self.$activeFqid
                        }),
                        contentType: "application/json; charset=UTF-8",
                        success: function (response) {
                            if (response.successOperation) {
                                var refresh = setTimeout(function () {
                                    location.reload();
                                }, 1000);
                                Alert(undefined, refresh);
                            }
                        }
                    });
                }
            })


            self.$btnDownloadGetWellProgram.on('click', function (e) {
                //e.preventDefault();

                var deferred = $.Deferred();

                Confirmation("Are you sure to generate this list?", deferred);

                $.when(deferred).done(function (answer) {
                    if (answer == "yes")
                        if (self.$supplierIDsGet.length == 0 || self.$supplierIDsGet.length == self.$getSupplierCount) {
                            window.open(self.pageUrl + "/ExportGetWellProgram", "_blank");
                        }

                        else {
                            window.open(self.pageUrl + "/ExportGetWellProgramBySelected?IDs=" + self.$supplierIDsGet , "_blank");
                        }
                });
            });
        },
        changeTableHeader: function (points) {
            var self = this;

            //$.each(points, function (key, value) {
            //    $("#thpoint" + key).text(value);
            //});

            for (var i = 1; i <= 4; i++) {
                $("#thpoint" + i).text("");
                if (points[i] != undefined) {
                    $("#thpoint" + i).text(points[i])
                }
            }
        },
        setDataTableEvents: function() {
            var self = this;
            
            self.$getWellTable.DataTable({
                "aaSorting": [],
                "preDrawCallback": function (settings, json) {
                    $.each($("#getWellTbody tr").find('#chkGetWell'), function (i, obj) {
                        $(obj).off("change").on("change", function () {
                            var isChecked = $(this).is(':checked'),
                                buid = $(this).data('buid'),
                                supplierid = $(this).data('supplierid'),
                                fyid = $(this).data('fyid'),
                                fqid = $(this).data('fqid'),
                                getwellid = $(this).data('getwellid'),

                                obj = { isChecked: isChecked, businessunitId: buid, supplierId: supplierid, fyId: fyid, fqId: fqid, getwellId: getwellid };

                            if (getwellid != undefined) {
                                //var index = self.$getWellObjectsToPost.findIndex(function (gw) {
                                //    return gw.businessunitId == buid && gw.supplierId == supplierid && gw.fyId == fyid && gw.fqId == fqid && gw.getwellId == getwellid;
                                //});

                                debugger;

                                var index = -1;

                                $.each(self.$getWellObjectsToPost, function (i, obj) {
                                    if (obj.businessunitId == buid && obj.supplierId == supplierid && obj.fyId == fyid && obj.fqId == fqid && obj.getwellId == getwellid)   index = i;
                                });
                                    
                                if (index > -1) {
                                    self.$getWellObjectsToPost.splice(index, 1)
                                }
                                else {
                                    self.$getWellObjectsToPost.push(obj);
                                }
                            }
                            else {
                                //var index = self.$getWellObjectsToPost.findIndex(function (gw) {
                                //    return gw.businessunitId == buid && gw.supplierId == supplierid && gw.fyId == fyid && gw.fqId == fqid;
                                //});
                                debugger;

                                var index = -1;

                                $.each(self.$getWellObjectsToPost, function (i, obj) {
                                    if (obj.businessunitId == buid && obj.supplierId == supplierid && obj.fyId == fyid && obj.fqId == fqid) index = i;
                                });
                                
                                index > -1 ?
                                    self.$getWellObjectsToPost.splice(index, 1) :
                                    self.$getWellObjectsToPost.push(obj);
                            }

                            self.$btnSaveGetWellProgram.show()

                       
                            if (self.$getWellObjectsToPost.length > 0) {
                                //self.$btnClearGetWellProgram.hide();

                                self.$btnSaveGetWellProgramText.text("Add/Update in Get Well Program")
                                self.$willClear = false;

                            }
                            else {
                                //self.$btnSaveGetWellProgram.hide();

                                self.$btnSaveGetWellProgramText.text("Clear Get Well Program")
                                self.$willClear = true;

                            }
                              
                        });
                    });
                },
                "columnDefs": [
                    {
                        "targets": [0],
                        "orderable": false
                    }
                ]
            });
        },
        rebuildGetWellTable: function(response, deferred) {
            var self = this;

            self.$getWellTable.DataTable().destroy();

            self.$getWellTableTbody.empty();

            $.each(response.gwps, function (ind, obj) {
                var html = "<tr>" +
                    "<td>" + self.generateCheckbox(obj) + " </td>" +
                    "<td>" + obj.SupplierMaster.SupplierName + "</td>" +
                    "<td>" + obj.SupplierMaster.SupplierBuyingRegion.RegionDescription + "</td>" +
                    "<td>" + obj.SupplierMaster.SupplierCategory.CategoryName + "</td>" +
                    "<td>" + obj.BusinessUnit.BUName + "</td>" +
                    "<td>" + Math.round(obj.Point1) + "%</td>" +
                    "<td>" + Math.round(obj.Point2) + "%</td>" +
                    "<td>" + Math.round(obj.Point3) + "%</td>" +
                    "<td>" + Math.round(obj.Point4) + "%</td>"
                "</tr>";
                self.$getWellTableTbody.append(html);
            });
            deferred.resolve();
        },
        generateCheckbox: function(obj) {
            var html = "";
            var self = this;

            if (obj.ExistingGetWellProgramID > 0) {
                if (self.$roleId == 3) {
                    html = "<input type='checkbox' id='chkGetWell' data-buid='" + obj.BusinessUnit.BusinessUnitID + "' data-supplierId='" + obj.SupplierMaster.SupplierID + "' data-fyid='" + obj.FiscalYear.FYID + "' data-fqid='" + obj.FiscalQuarter.FQID + "' data-getwellid='" + obj.ExistingGetWellProgramID + "' checked disabled/>";
                }
                else {
                    html = "<input type='checkbox' id='chkGetWell' data-buid='" + obj.BusinessUnit.BusinessUnitID + "' data-supplierId='" + obj.SupplierMaster.SupplierID + "' data-fyid='" + obj.FiscalYear.FYID + "' data-fqid='" + obj.FiscalQuarter.FQID + "' data-getwellid='" + obj.ExistingGetWellProgramID + "' checked />";
                }
            }
            else {
                if (self.$roleId == 3) {
                    html = "<input type='checkbox' id='chkGetWell' data-buid='" + obj.BusinessUnit.BusinessUnitID + "' data-supplierId='" + obj.SupplierMaster.SupplierID + "' data-fyid='" + obj.FiscalYear.FYID + "' data-fqid='" + obj.FiscalQuarter.FQID + "' disabled/>";
                }
                else {
                    html = "<input type='checkbox' id='chkGetWell' data-buid='" + obj.BusinessUnit.BusinessUnitID + "' data-supplierId='" + obj.SupplierMaster.SupplierID + "' data-fyid='" + obj.FiscalYear.FYID + "' data-fqid='" + obj.FiscalQuarter.FQID + "' />";
                }
            }
            return html;
        }
    }

    var InitializeGetWell = function () {
        var getWellTaskObj = Object.create(getWellTask);
        getWellTaskObj.init();
     
    }
    InitializeGetWell();
});