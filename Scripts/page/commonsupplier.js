﻿$(document).ready(function () {
    var CommonSupplierTask = {
        init: function () {
            var self = this;
            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.$table = $("#tblCommonSuppplier");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnGenerate = $("#btnGenerate");

            // VARIABLES
            self.fromGenerateYn = false;
            self.firstPageYn = true;
            self.imageDatas = [];
            self.globalDeferred = $.Deferred();
            self.maxPageNo = 0;
            self.currPageNo = 0;
        },

        setEvents: function () {
            var self = this;

            self.getCommonSuppliers();
        },

        getCommonSuppliers: function () {
            var self = this;

            $.get(self.pageUrl + "/GetCommonSuppliers", { fyid: 0, fqid: 0 }, function (data) {
                var $tblHeader = self.$table.find("thead > tr"), $tblBody = self.$table.find("tbody");
                var columnCtr = $tblHeader.find("th").length;

                $.each(data.CommonSuppliers, function (ind, commonSupplier) {
                    var htmltx = "";
                    var $existingRow = $tblBody.find("tr[data-supplier-id=" + commonSupplier.SupplierID + "]");

                    var value = commonSupplier.SupplierName;
                    for (ctr = 0; ctr < columnCtr; ctr++) {
                        var ptermsyn = false;
                        var pnetdaysno = 0;
                        if (ctr > 0) {
                            value = "";
                            var $columnElement = $tblHeader.find("th:eq(" + ctr + ")");
                            if ($columnElement.data("bu-id") != undefined) {
                                if (parseInt($columnElement.data("bu-id")) == commonSupplier.BusinessUnitID) {
                                    value = '<i class="fa fa-check"></i>';
                                }
                            } else if ($columnElement.data("ptermsbu-id") != undefined) {
                                if (parseInt($columnElement.data("ptermsbu-id")) == commonSupplier.BusinessUnitID) {
                                    value = commonSupplier.PDescription;
                                    ptermsyn = true;
                                    pnetdaysno = commonSupplier.PNetDaysToPay;
                                }
                            }
                        }

                        if ($existingRow.length == 0) {
                            if (ptermsyn) htmltx += ("<td data-pnetdays-no='"+pnetdaysno+"'>" + value + "</td>");
                            else htmltx += ("<td>" + value + "</td>");
                        } else {
                            if (ctr > 0) {
                                $existingRow.find("td:eq(" + ctr + ")").append(value);// .text(value);
                                if (ptermsyn) {
                                    if ($existingRow.find("td:eq(" + ctr + ")").data("pnetdays-no") == undefined)
                                        $existingRow.find("td:eq(" + ctr + ")").attr("data-pnetdays-no", pnetdaysno);
                                }
                            }
                        }
                    }

                    if ($existingRow.length == 0) $tblBody.append("<tr role='row' data-supplier-id='" + commonSupplier.SupplierID + "'>" + htmltx + "</tr>");

                });

                var atargets = [];

                //for (i = 1; i < $tblHeader.find("th").length; i += 2) {
                //    atargets.push(i);
                //}

                for (i = 1; i < $tblHeader.find("th").length; i += 1) {
                    atargets.push(i);
                }

                self.$table.DataTable({
                    "scrollX": true,
                    "bSort": true,
                    "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': atargets
                    }
                    ],
                    "initComplete": function (settings, json) {
                        setTimeout(function () {
                            //alert('DataTables has finished its initialisation.');
                            self.highlightCell();
                        })
                    }
                });

            }).fail(function () {
                alert("Data cannot be retrieve");
            });

            self.$btnGenerate.on("click", function () {
                $.get(self.pageUrl + "/GenerateCommonSupplierReport", {}, function (returnData) {
                    if (returnData.DownloadPath != undefined && returnData.DownloadPath.length > 0) {
                        window.location.href = '/CommonSupplier/DownloadPPT?fileinfo=' + returnData.DownloadPath;
                    }
                    HideLoading();
                }).fail(function (returnData) {
                    HideLoading();
                });
            });

            self.$table.on('page.dt', function () {
                setTimeout(function () {
                    self.highlightCell();
                });
            });
        },

        highlightCell: function ($element, deferred) {
            var self = this;
            $.each(self.$table.find("tr"), function (ind, obj) {
                var $tr = $(obj);
                var loopCtr = $tr.find("td[data-pnetdays-no]").length;
                var maxPnetdays = 0, indexMaxPnetdays = [];

                if (loopCtr > 0) {
                    for (i = 0; i < loopCtr; i++) {
                        var $td = $($tr.find("td[data-pnetdays-no]")[i]);
                        var currentPnetDaysNo = parseInt($td.data("pnetdays-no"));
                        if (currentPnetDaysNo > maxPnetdays) {
                            maxPnetdays = currentPnetDaysNo;
                            indexMaxPnetdays = $td.index();
                            indexMaxPnetdays = [];
                            indexMaxPnetdays[0] = $td.index();
                        } else if (currentPnetDaysNo == maxPnetdays) {
                            indexMaxPnetdays[indexMaxPnetdays.length] = $td.index();
                        }
                    }
                }

                if (indexMaxPnetdays.length > 0) {
                    $.each(indexMaxPnetdays, function (ind, value) {
                        //var rgb = indexMaxPnetdays.length > 1 ? "rgb(125, 234, 42)" : "lightblue";
                        var rgb = "rgb(125, 234, 42)";
                        var $currenttd = $tr.find("td:eq(" + value + ")");
                        var currentText = $currenttd.text();
                        $currenttd.css("background", rgb);
                        $currenttd.text("");
                        $currenttd.append(currentText);
                    })
             
                }
            })
        },
    }

    var InitializeCommonSupplierTask = function () {
        var commonSupplierTaskObj = Object.create(CommonSupplierTask);
        commonSupplierTaskObj.init();
    }

    InitializeCommonSupplierTask();
});