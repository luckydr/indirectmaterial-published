﻿$(document).ready(function () {
    // supplier object
    var SupplierObjTask = {
        init: function () {
            var self = this;
            var deferred = $.Deferred();

            self.populateBUAdmins(deferred);
            self.declaration();
            self.setEvents();
            $.when(deferred).done(function () {
                self.setDataTableEvents();
            });
        },
        initializeDatepicker: function () {
            $("input#contractStartDate, input#contractEndDate").datepicker();
        },
        populateBUAdmins: function (deferred) {
            var self = this;

            $.ajax({
                url: GetAppPath() + "Employee/GetBUAdmins",
                type: "GET",
                dataType: "json",
                success: function (response) {
                    self.$buAdmins = response.admins;
                    deferred.resolve();
                }
            });
        },
        initializePopover: function () {
            var self = this;

            $.each($('span#BUContact'), function (i, obj) {
                $(this).popover();

                var html = "";

                $.each(self.$buAdmins[$(this).data('buid')], function (ind, arr) {
                    html += "<a href=mailto:" + arr.EmailAddress + ">" + arr.FullName + "</a><br>";
                });

                $(this).attr('data-content', html);
            });
        },
        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");

            self.$supplierTrows = $("#supplierTbody tr");
            self.$btnAdd = $("#btnAddSupplier");
            self.$frmSupplier = $("#frmSupplier");
            self.$supplierTable = $('#supplierTable');
            self.$supplierTbody = $("#supplierTbody");
            self.$supplierStatusID = $("#statusID");
            self.$fillAllFieldsDiv = $("#fillAllFieldsDiv");
            self.$successMessage = $("#successMessage");
            self.$supplierErrorUL = $("#supplierErrorUL");
            self.$fillAllFieldsDiv = $("#fillAllFieldsDiv");
            self.$buAdmins = {};
            self.$btnDownloadSupplier = $("#btnDownloadSupplier");

            self.$isModalOpen = false;
            self.successyn = false;
        },
        populateDataInSupplierModal: function (url, deferred, operation, buid) {
            $.ajax({
                type: "GET",
                url: url,
                data: {
                    operation: operation,
                    buid: buid
                },
                success: function (data) {
                    deferred.resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },
        editSupplierData: function (id, operation, deferredPartialViewObjInit, buid) {
            var self = this;
            var deferred = $.Deferred();

            $.when(deferred).done(function (partialviewhtmltx) {
                var afterShowEventFunc = [];
                var onModalCloseEventFunc = [];

                afterShowEventFunc.push(function () {
                    $("#btnSave").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplier();
                    });
                });

                onModalCloseEventFunc.push(function () {
                    self.$isModalOpen = !self.$isModalOpen;
                    if (self.successyn) {
                        self.successyn = false;
                        Alert();
                    }
                });

                $(this).modalTask({
                    id: "AddEditSupplierModal",
                    title: operation == "view" ? "View Supplier" : "Edit Supplier",
                    dataHtml: partialviewhtmltx,
                    eventsAfterShow: afterShowEventFunc,
                    eventsCloseModal: onModalCloseEventFunc,
                    btnYesHideyn: true,
                    btnNoHideyn: true
                });
                deferredPartialViewObjInit.resolve();
            });
            self.populateDataInSupplierModal(self.pageUrl + '/AddEditSupplier/' + id, deferred, operation, buid);
        },
        processSaveSupplier: function () {
            var self = this;

            ShowLoading();

            $.ajax({
                url: self.pageUrl + "/Post",
                type: "POST",
                data: $("#frmSupplier").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.Data) {
                        self.successyn = true;
                        $("#AddEditSupplierModal").modal('toggle');
                        self.setDataTableEvents();
                        HideLoading();
                    }
                    else {
                        self.clearValidationErrors();

                        var duplicateErrors = "";

                        $.each(response.Errors, function (key, value) {
                            //$("#" + key + "Error").text("*").css({ "color": "red", "font-weight": "bold" });//.addClass("font4");         
                            if (key.indexOf("exist") > -1 || key.indexOf("error") > -1 || key.indexOf("invalid") > -1) {
                                duplicateErrors += value + ',';
                            }
                            else {
                                $("#" + key + "Error").addClass("spvalidation");
                                $("#fillAllFieldsDiv").show();
                            }
                        });

                        if (duplicateErrors != "") {
                            $.each(duplicateErrors.split(","), function (i, obj) {
                                if (obj != "") {
                                    $("#supplierErrorUL").append("<li>" + obj + "</li>");
                                }
                            });
                            $("#supplierErrorUL").get(0).scrollIntoView();
                        }
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        setEvents: function () {
            var self = this;

            self.$supplierStatusID.on("change", function () {
                ShowLoading();
                var statusID = $(this).val();
                self.filterSupplierByStatus(statusID);
            });

            self.$btnDownloadSupplier.on('click', function (e) {
                //e.preventDefault();

                var deferred = $.Deferred();

                Confirmation("Are you sure to generate all of the supplier?", deferred);

                $.when(deferred).done(function (answer) {
                    if (answer == "yes")
                        window.open(self.pageUrl + "/ExportSupplier", "_blank");
                });
            });
            self.$btnAdd.on("click", function (event) {
                if (!self.$isModalOpen) {
                    self.$isModalOpen = !self.$isModalOpen;
                    event.preventDefault();
                    var deferred = $.Deferred();
                    var deferredPartialViewInit = $.Deferred();
                    $.when(deferred).done(function (partialviewhtmltx) {
                        var afterShowEventFunc = [];
                        var onModalCloseEventFunc = [];

                        afterShowEventFunc.push(function () {
                            $("#btnSave").on('click', function (e) {
                                e.preventDefault();
                                self.processSaveSupplier();
                            });
                        });

                        onModalCloseEventFunc.push(function () {
                            self.$isModalOpen = !self.$isModalOpen;

                            if (self.successyn) {
                                self.successyn = false;
                                Alert();
                            }
                        });

                        $(this).modalTask({
                            id: "AddEditSupplierModal",
                            title: "Add Supplier",
                            dataHtml: partialviewhtmltx,
                            eventsAfterShow: afterShowEventFunc,
                            eventsCloseModal: onModalCloseEventFunc,
                            btnYesHideyn: true,
                            btnNoHideyn: true
                        });
                        deferredPartialViewInit.resolve();
                    });
                    $.when(deferredPartialViewInit).done(function () {
                        initPartialViewObj();
                        self.initializeDatepicker();
                    });
                    self.populateDataInSupplierModal(self.pageUrl + '/AddEditSupplier/0', deferred, "add", 0);
                    //e.preventDefault(event);
                }
            });
        },
        setDataTableEvents: function () {
            var self = this;

            $("#supplierTable").DataTable().destroy();

            $("#supplierTable").DataTable({
                "order": [],
                "searchDelay": 500,
                "bServerSide": true,
                "ajax": GetAppPath() + $("div[data-maincontainer-yn]").data("page-url") + "/AjaxList",
                "sPaginationType": "simple_numbers",
                "bFilter": true,
                "iDisplayLength": 10,
                "oLanguage": {
                    "sLengthMenu": " _MENU_ ",
                    "sInfo": "_START_ to _END_ of _TOTAL_ records",
                    "sInfoEmpty": "",
                    "sInfoFiltered": ""
                },
                "aoColumnDefs": [
                    {
                        "aTargets": [0],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<a href='#' class='a-edit' data-id='" + columns[9] + "' data-buid='" + columns[10] + "'>" + "<i class='fa fa-pencil-square-o'></i>" + data + "</a>";
                        }
                    },
                    {
                        "aTargets": [1],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [2],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<div class='BUContacts'>" + "<span id='BUContact' data-buid='" + columns[10] + "' style='font-size:80%;cursor:pointer' title='BU Admin' data-container='body' data-toggle='popover' data-placement='top' data-html='true' data-content='N/A' tabindex='0' role='button' data-trigger='focus'><i class='fa fa-info-circle'></i></span>" + data + "</div>"
                        }
                    },
                    {
                        "aTargets": [3],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [4],
                        "bSortable": true,
                        "bSearchable": true
                    },
                    {
                        "aTargets": [5],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<span>" + data + "%</span>";
                        }
                    },
                    {
                        "aTargets": [6],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<span>" + data + "%</span>";
                        }
                    },
                    {
                        "aTargets": [7],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<span>" + data + "%</span>";
                        }
                    },
                    {
                        "aTargets": [8],
                        "bSortable": true,
                        "bSearchable": true,
                        "mRender": function (data, display, columns) {
                            return "<span>" + data + "%</span>";
                        }
                    },
                ],
                "preDrawCallback": function () {
                    ShowLoading();
                },
                "drawCallback": function () {
                    HideLoading();
                    $.each($("#supplierTable tbody").find('.a-edit'), function (i, obj) {
                        $(obj).off('click').on('click', function (e) {
                            self.$isModalOpen = !self.$isModalOpen;
                            e.preventDefault();
                            ShowLoading();
                            var dataID = $(this).data('id');
                            var deferredPartialViewObjInit = $.Deferred();
                            self.editSupplierData(dataID, "edit", deferredPartialViewObjInit, $(this).data('buid'));
                            $.when(deferredPartialViewObjInit).done(function () {
                                initPartialViewObj();
                                self.initializeDatepicker();
                                HideLoading();
                            });
                        })
                    });
                    self.initializePopover();
                }
            });
        },
        filterSupplierByStatus: function (statusID) {
            var self = this;

            $.ajax({
                type: "GET",
                url: self.pageUrl + '/FilterSupplier',
                dataType: 'json',
                data: {
                    statusID: statusID
                },
                success: function (response) {
                    var deferred = $.Deferred();
                    self.rebuildSupplierDataTable(response, deferred);
                    $.when(deferred).done(function () {
                        self.setDataTableEvents();
                    });
                }
            })
        },
        clearValidationErrors: function () {
            var self = this;

            $.each($("form#frmSupplier span"), function (i, obj) {
                if ($(this).hasClass("spvalidation"))
                    $(this).removeClass("spvalidation");
            });
            $("#supplierErrorUL").empty();
            $("#fillAllFieldsDiv").hide();
        }
    }

    var InitializedSupplierObjTask = function () {
        var supplierObjTask = Object.create(SupplierObjTask);
        supplierObjTask.init();
    }
    InitializedSupplierObjTask();
    // end supplier obj

    // partial view obj
    var partialView = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.updateButtons();
        },
        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnEdit = $("#btnEdit");
            self.$btnSave = $("#btnSave");
            self.$btnCancel = $("#btnCancel");
            self.$operation = $("#Operation").val();
            self.$supplierID = $("#SupplierId").val();
            self.$supplierBUID = $("#supplierBUID").val();

            self.$supplierButtonTab = $("#supplierButtonTab");
            self.$supplierBusinessUnitMappingButtonTab = $("#supplierBusinessUnitMappingButtonTab");
            self.$supplierPaymentTermsButtonTab = $("#supplierPaymentTermsButtonTab");
            self.$supplierExpenseButtonTab = $("#supplierExpenseButtonTab");
            self.$supplierScoreButtonTab = $("#supplierScoreButtonTab");

            self.$supplierBusinessUnitMappingRecords = $("#supplierBusinessUnitMappingRecords");
            self.$supplierPaymentTermsRecords = $("#supplierPaymentTermsRecords");
            self.$supplierExpenseRecords = $("#supplierExpenseRecords");
            self.$supplierScoreRecords = $("#supplierScoreRecords");

            self.$AddEditSupplierPaymentTerms = $("#AddEditSupplierPaymentTerms");
            self.$AddEditSupplierExpense = $("#AddEditSupplierExpense");

            self.$supplierLabels = $("div.supplierLabels, div#supplierHorizontalTab");
            self.$ModalHeaderHeight = $("div.modalheaderheight");

            self.$cboBuyingRegion = $("#cboBuyingRegion");
            self.$cboBuyingLocation = $("#cboBuyingLocation");

            self.$supplierPaymentTermsTable = $("#supplierPaymentTermsTable");
            self.$supplierPaymentTermsTbody = $("#supplierPaymentTermsTbody");

            self.$supplierExpenseTable = $("#supplierExpenseTable");
            self.$supplierExpenseTbody = $("#supplierExpenseTbody");

            self.$supplierErrorUL = $("#supplierErrorUL");
            self.$fillAllFieldsDiv = $("#fillAllFieldsDiv");
            self.$editrquired = $(".rquired");
        },
        setEvents: function () {
            var self = this;

            if (self.$operation == "edit") {
                self.$btnCancel.on('click', function (e) {
                    e.preventDefault();
                    self.updateForms(true);
                    self.$btnSave.hide();
                    self.$btnEdit.show();
                    self.$editrquired.hide();
                    $(this).hide();
                    self.clearValidationErrors();
                    self.refreshValue();
                });
            }
            self.$btnEdit.on('click', function (e) {
                e.preventDefault();
                self.updateForms(false);
                $(this).hide();
                self.$btnSave.show();
                self.$btnCancel.show();
                self.$editrquired.show();
            });
            self.$supplierButtonTab.click(function (e) {
                e.preventDefault();
                self.initializeTabEvents("supplier");
            });
            self.$supplierBusinessUnitMappingButtonTab.click(function (e) {
                e.preventDefault();
                self.initializeTabEvents("supplierBusinessUnitMapping");
            });
            self.$supplierPaymentTermsButtonTab.click(function (e) {
                e.preventDefault();
                self.initializeTabEvents("supplierPaymentTerms");
            });
            self.$supplierExpenseButtonTab.click(function (e) {
                e.preventDefault();
                self.initializeTabEvents("supplierExpense");
            });
            self.$supplierScoreButtonTab.click(function (e) {
                e.preventDefault();
                self.initializeTabEvents("supplierScore");
            });
            self.$cboBuyingRegion.on("change", function (e) {
                if ($(this).val() != "") {
                    ShowLoading();
                    $.ajax({
                        type: "GET",
                        url: self.pageUrl + "/GetSupplierBuyingLocation",
                        data: {
                            regionId: $(this).val()
                        },
                        dataType: 'json',
                        success: function (response) {
                            HideLoading();

                            self.$cboBuyingLocation.find('option').not(':first').remove();

                            $.each(response.locations, function (i, obj) {
                                var html = "<option value=" + obj.Value + ">" + obj.Text + "</option>";
                                self.$cboBuyingLocation.append(html);
                            });
                        }
                    })
                }
            });
        },
        updateButtons: function () {
            var self = this;

            switch (self.$operation) {
                case "view":
                    self.$btnSave.hide();
                    self.$btnEdit.hide();
                    self.$btnCancel.hide();
                    self.updateForms(true);
                    break;
                case "edit":
                    self.$editrquired.hide();
                    self.$btnSave.hide();
                    self.$btnCancel.hide();
                    self.updateForms(true);
                    break;
                case "add":
                    self.updateForms(false);
                    self.$btnEdit.hide();
                    self.$btnCancel.hide();
                    self.$supplierLabels.hide();
                    self.$supplierPaymentTermsButtonTab.hide();
                    self.$supplierBusinessUnitMappingButtonTab.hide();
                    self.$supplierScoreButtonTab.hide();
                    self.$supplierExpenseButtonTab.hide();
                    self.$ModalHeaderHeight.removeClass();
                    break;
                default: break;
            }
        },
        initializeTabEvents: function (module) {
            var self = this;

            if (self.$operation == "edit") {
                self.$btnEdit.hide();
                self.$btnSave.hide();
                self.$btnCancel.hide();

                $.each($("#buttonTabs .tablinks"), function () {
                    $(this).removeClass("active");
                    $(this).removeClass("selected");
                });
                $.each($("div.tabcontent"), function () {
                    $(this).css("display", "none");
                });

                $("#" + module + "ButtonTab").addClass("active");
                $("#" + module + "ButtonTab").addClass("selected");
                $("#" + module + "Tab").show();

                switch (module) {
                    case "supplier":
                        self.$btnEdit.show();
                        self.updateForms(true);
                        break;
                    case "supplierPaymentTerms":
                        initSupplierPaymentTerms();
                        self.$supplierPaymentTermsRecords.show();
                        self.$AddEditSupplierPaymentTerms.hide();
                        break;
                    case "supplierExpense":
                        initSupplierExpense();
                        self.$supplierExpenseRecords.show();
                        self.$AddEditSupplierExpense.hide();
                        break;
                    case "supplierBusinessUnitMapping":
                        initSupplierBUMapping();
                        self.$supplierBusinessUnitMappingRecords.show();
                        break;
                    default: break;
                }
            }
        },
        updateForms: function (disabled) {
            var self = this;

            $.each($("form#frmSupplier :input").not(".btn"), function () {
                $(this).prop("disabled", disabled);
            });
        },
        refreshValue: function () {
            var self = this;

            $.each($("form#frmSupplier input[type=text]"), function (i, obj) {
                $(this).val($(this).attr('value'));
            });
            $.each($("form#frmSupplier select"), function (i, obj) {
                if ($(this).find('option[selected]').length > 0)
                    $(this).val($(this).find('option[selected]')[0].value);
                else
                    $(this).val("");
            });
        },
        clearValidationErrors: function () {
            var self = this;

            $.each($("form#frmSupplier span"), function (i, obj) {
                if ($(this).hasClass("spvalidation")) $(this).removeClass("spvalidation");
            });
            self.$fillAllFieldsDiv.hide();
            self.$supplierErrorUL.empty();
        }
    };
    var initPartialViewObj = function () {
        var partialViewObj = Object.create(partialView);
        partialViewObj.init();
    }
    // end partial view obj

    // supplier bu mapping obj
    var supplierBUMapping = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.setDataTableEvents();
        },
        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnAdd = $("#btnAddSupplierBusinessUnitMapping");
            self.$btnAddSystemAdmin = $("#btnAddSupplierBusinessUnitMappingSystemAdmin");
            self.$SupplierId = $("#SupplierId").val();
            self.$userLoggedInBUID = $("#userLoggedInBUID").val();
            self.$userLoggedInRoleID = $("#userLoggedInRoleID").val();
            self.$AddEditSupplierBusinessUnitMapping = $("#AddEditSupplierBusinessUnitMapping");
            self.$supplierBusinessUnitMappingRecords = $("#supplierBusinessUnitMappingRecords");
            self.$supplierBusinessUnitMappingTable = $("#supplierBusinessUnitMappingTable");
            self.$supplierBusinessUnitMappingTableTbody = $("#supplierBusinessUnitMappingTbody");
            self.$supplierBusinessUnitMappingTableTr = $("#supplierBusinessUnitMappingTbody tr");
            self.$supplierScoreThumbnail = $("#supplierScoreThumbnail");
            self.$supplierPaymentTermsButtonTab = $("#supplierPaymentTermsButtonTab");
            self.$supplierExpenseButtonTab = $("#supplierExpenseButtonTab");
            self.$supplierScoreButtonTab = $("#supplierScoreButtonTab");
        },
        setEvents: function () {
            var self = this;

            self.$btnAdd.off('click').on('click', function () {
                //ShowLoading();
                //var deferred = $.Deferred();
                //self.AddEditSupplierBUMapping(0, self.$SupplierId, deferred);
                //$.when(deferred).done(function () {
                //    HideLoading();
                //    $("#btnSaveSupplierBUMapping").on('click', function (e) {
                //        e.preventDefault();
                //        self.processSaveSupplierBUMapping();
                //    });
                //    $("#btnCancelSupplierBUMapping").on('click', function (e) {
                //        e.preventDefault();
                //        self.$AddEditSupplierBusinessUnitMapping.hide();
                //        self.$supplierBusinessUnitMappingRecords.show();
                //    });
                //})
                ShowLoading();

                $.ajax({
                    url: GetAppPath() + "SupplierBusinessUnitMapping/AddMyBusinessUnit",
                    type: "POST",
                    data: JSON.stringify({
                        supplierId: self.$SupplierId
                    }),
                    contentType: "application/json; charset=UTF-8",
                    dataType: "json",
                    success: function (response) {
                        HideLoading();

                        if (response.successOperation) {
                            SupplierObjTask.init();
                            self.$btnAdd.hide();
                            self.rebuildSupplierBUMapping(response.bus);
                            self.setDataTableEvents();
                            self.$AddEditSupplierBusinessUnitMapping.hide();
                            self.$supplierBusinessUnitMappingRecords.show();
                        }
                    },
                    error: function (response) {
                        HideLoading();
                    }
                })
            });

            self.$btnAddSystemAdmin.off('click').on('click', function (e) {
                ShowLoading();

                var deferred = $.Deferred();
                self.AddEditSupplierBUMapping(0, self.$SupplierId, deferred);
                $.when(deferred).done(function () {
                    HideLoading();
                    $("#btnSaveSupplierBUMapping").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplierBUMapping();
                    });
                    $("#btnCancelSupplierBUMapping").on('click', function (e) {
                        e.preventDefault();
                        self.$AddEditSupplierBusinessUnitMapping.hide();
                        self.$supplierBusinessUnitMappingRecords.show();
                    });
                });
            })
        },
        AddEditSupplierBUMapping: function (id, supplierId, deferred) {
            var self = this;

            $.ajax({
                type: "GET",
                url: GetAppPath() + "SupplierBusinessUnitMapping/AddEditSupplierBusinessUnit/" + id,
                data: {
                    supplierId: supplierId
                },
                success: function (response) {
                    self.$AddEditSupplierBusinessUnitMapping.html(response).show();
                    self.$supplierBusinessUnitMappingRecords.hide();
                    deferred.resolve();
                }
            })
        },
        processSaveSupplierBUMapping: function () {
            var self = this;

            ShowLoading();

            $.ajax({
                type: "POST",
                url: GetAppPath() + "SupplierBusinessUnitMapping/Post",
                data: $("#frmSupplierBUMapping").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.successOperation) {
                        self.rebuildSupplierBUMapping(response.bus);
                        self.setDataTableEvents();
                        SupplierObjTask.init();
                        self.$AddEditSupplierBusinessUnitMapping.hide();
                        self.$supplierBusinessUnitMappingRecords.show();
                    }
                    else {
                        self.clearErrors();
                        self.populateErrors(response.Errors);
                        HideLoading();
                    }
                }
            });
        },
        clearErrors: function () {
            $("#SBUMBusinessUnitError").text("");
            $("#SBUMStatusError").text("");
            $("#sbmuerrorUL").empty();
        },
        populateErrors: function (errors) {
            var self = this;

            $.each(errors, function (index, obj) {
                $("#" + index + "Error").text(obj);
                if (index == "sbmuDuplicate") $("#sbmuerrorUL").append("<li>" + obj + "</li>");
            });
        },
        rebuildSupplierBUMapping: function (businessUnits) {
            var self = this;
            var activeBuCtr = 0;

            self.$supplierBusinessUnitMappingTable.DataTable().destroy();

            self.$supplierBusinessUnitMappingTableTbody.empty();

            self.$supplierScoreThumbnail.empty();

            $.each(businessUnits, function (i, obj) {
                var firstTd = obj.BusinessUnitID == self.$userLoggedInBUID || self.$userLoggedInRoleID == 1 ? "<td>" + "<a href='#' class='a-edit' data-id=" + obj.SupplierBUID + ">" + obj.BusinessUnit.BUName + "</a></td>" : "<td class='unclickable'>" + obj.BusinessUnit.BUName + "</td>";
                var htmlTr = "<tr>" +
                    firstTd +
                    "<td>" + obj.StatusList.StatusDescription + "</td>" +
                    "</tr>";

                self.$supplierBusinessUnitMappingTableTbody.append(htmlTr);

                if (obj.StatusList.StatusID == 1) {
                    var htmlSsTn = "<div class='col-xs-12 col-sm-4'>" +
                        "<a href="+ $("#hostname").val()+"/SupplierScore/Index?supplierID=" + self.$SupplierId + "&businessUnitID=" + obj.BusinessUnit.BusinessUnitID + " target='_blank'>" + obj.BusinessUnit.BUName +
                        "</a> </div>";
                    self.$supplierScoreThumbnail.append(htmlSsTn);

                    activeBuCtr++;
                }
            });

            if (activeBuCtr > 0) {
                self.$supplierPaymentTermsButtonTab.parent().show();
                self.$supplierExpenseButtonTab.parent().show();
                self.$supplierScoreButtonTab.parent().show();
            }
            else {
                self.$supplierPaymentTermsButtonTab.parent().hide();
                self.$supplierExpenseButtonTab.parent().hide();
                self.$supplierScoreButtonTab.parent().hide();
            }

            HideLoading();
        },
        setDataTableEvents: function () {
            var self = this;

            self.$supplierBusinessUnitMappingTable.DataTable().destroy();

            self.$supplierBusinessUnitMappingTable.DataTable({
                "aaSorting": [],
                "preDrawCallback": function (settings, json) {
                    $.each(self.$supplierBusinessUnitMappingTableTbody.find('.a-edit'), function (i, obj) {
                        $(obj).off('click').on('click', function (e) {
                            ShowLoading();
                            e.preventDefault();
                            var deferred = $.Deferred();
                            self.AddEditSupplierBUMapping($(this).data('id'), self.$SupplierId, deferred);
                            $.when(deferred).done(function () {
                                HideLoading();
                                $("#btnSaveSupplierBUMapping").on('click', function (e) {
                                    e.preventDefault();
                                    self.processSaveSupplierBUMapping();
                                });
                                $("#btnCancelSupplierBUMapping").on('click', function (e) {
                                    e.preventDefault();
                                    self.$AddEditSupplierBusinessUnitMapping.hide();
                                    self.$supplierBusinessUnitMappingRecords.show();
                                });
                            });
                        })
                    });
                }
            });
        }
    }
    var initSupplierBUMapping = function () {
        var supplierBUMappingObj = Object.create(supplierBUMapping);
        supplierBUMappingObj.init();
    }
    // end supplier bu mapping obj

    // supplier payment terms obj
    var supplierPaymentTerms = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.setDataTableEvents();
        },
        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnAddSupplierPaymentTerms = $("#btnAddSupplierPaymentTerms");
            self.$btnAddSupplierPaymentTermsSystemAdmin = $("#btnAddSupplierPaymentTermsSystemAdmin");
            self.$SupplierId = $("#SupplierId").val();
            self.$supplierBUID = $("#supplierBUID").val();
            self.$userLoggedInBUID = $("#userLoggedInBUID").val();
            self.$userLoggedInRoleID = $("#userLoggedInRoleID").val();
            self.$AddEditSupplierPaymentTerms = $("#AddEditSupplierPaymentTerms");
            self.$supplierPaymentTermsRecords = $("#supplierPaymentTermsRecords");
            self.$supplierPaymentTermsTable = $("#supplierPaymentTermsTable");
            self.$supplierPaymentTermsTbody = $("#supplierPaymentTermsTbody");
            self.$frmSupplierPaymentTerms = $("#frmSupplierPaymentTerms");
            self.isSystemAdmin = self.$userLoggedInRoleID == 1;
        },
        setEvents: function () {
            var self = this;

            self.$btnAddSupplierPaymentTerms.off('click').on('click', function (e) {
                e.preventDefault();
                ShowLoading();
                var deferred = $.Deferred();
                self.AddEditSupplierPaymentTerms(0, self.$SupplierId, self.$supplierBUID, self.isSystemAdmin, deferred);
                $.when(deferred).done(function () {
                    HideLoading();
                    $("#btnSaveSupplierPaymentTerms").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplierPaymentTerms();
                    });
                    $("#btnCancelSupplierPaymentTerms").on('click', function (e) {
                        e.preventDefault();
                        self.$AddEditSupplierPaymentTerms.hide();
                        self.$supplierPaymentTermsRecords.show();
                    });
                });
            });
            self.$btnAddSupplierPaymentTermsSystemAdmin.off('click').on('click', function (e) {
                e.preventDefault();
                var deferred = $.Deferred();
                self.AddEditSupplierPaymentTerms(0, self.$SupplierId, self.$supplierBUID, self.isSystemAdmin, deferred);
                $.when(deferred).done(function () {
                    HideLoading();
                    $("#btnSaveSupplierPaymentTerms").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplierPaymentTerms();
                    });
                    $("#btnCancelSupplierPaymentTerms").on('click', function (e) {
                        e.preventDefault();
                        self.$AddEditSupplierPaymentTerms.hide();
                        self.$supplierPaymentTermsRecords.show();
                    });
                });
            });
        },
        setDataTableEvents: function () {
            var self = this;

            self.$supplierPaymentTermsTable.DataTable().destroy();

            self.$supplierPaymentTermsTable.DataTable({
                "aaSorting": [],
                "preDrawCallback": function (settings, json) {
                    $.each(self.$supplierPaymentTermsTbody.find('.a-edit'), function (i, obj) {
                        $(obj).off('click').on('click', function (e) {
                            ShowLoading();
                            e.preventDefault();
                            var deferred = $.Deferred();
                            self.AddEditSupplierPaymentTerms($(this).data('id'), self.$SupplierId, $(this).data('buid'), self.isSystemAdmin, deferred);
                            $.when(deferred).done(function () {
                                HideLoading();
                                $("#btnSaveSupplierPaymentTerms").on('click', function (e) {
                                    e.preventDefault();
                                    self.processSaveSupplierPaymentTerms();
                                });
                                $("#btnCancelSupplierPaymentTerms").on('click', function (e) {
                                    e.preventDefault();
                                    self.$AddEditSupplierPaymentTerms.hide();
                                    self.$supplierPaymentTermsRecords.show();
                                });
                            });
                        })
                    });
                }
            });
        },
        AddEditSupplierPaymentTerms: function (id, supplierID, buid, isSystemAdmin, deferred) {
            var self = this;

            $.ajax({
                type: 'GET',
                url: GetAppPath() + 'SupplierPaymentTerms/AddEditSupplierPaymentTerms/' + id,
                data: {
                    supplierID: supplierID,
                    buid: buid,
                    isSystemAdmin: isSystemAdmin
                },
                success: function (response) {
                    self.$AddEditSupplierPaymentTerms.html(response).show();
                    self.$supplierPaymentTermsRecords.hide();
                    deferred.resolve();
                }
            });
        },
        rebuildSupplierPaymentTerms: function (spts) {
            var self = this;

            self.$supplierPaymentTermsTable.DataTable().destroy();

            self.$supplierPaymentTermsTbody.empty();

            $.each(spts, function (i, obj) {
                var firstTd = obj.BusinessUnitID == self.$userLoggedInBUID || self.$userLoggedInRoleID == 1 ? "<td>" + "<a href='#' class='a-edit' data-id='" + obj.SupplierPTID + "' data-buid='" + obj.BusinessUnitID + "'>" + obj.PaymentTerms.PDescription + "</a>" + "</td>" : "<td class='unclickable'>" + obj.PaymentTerms.PDescription + "</td>";
                var htmlTx = "<tr>" +
                    firstTd +
                    "<td>" + obj.BusinessUnit.BUName + "</td>" +
                    "<td>" + obj.FiscalYear.FYDescription + "</td>" +
                    "<td>" + obj.FiscalQuarter.FQDescription + "</td>" +
                    "</tr>";
                self.$supplierPaymentTermsTbody.append(htmlTx);
            });

            HideLoading();
        },
        processSaveSupplierPaymentTerms: function () {
            var self = this;

            ShowLoading();

            $.ajax({
                url: GetAppPath() + "SupplierPaymentTerms/Post",
                type: "POST",
                data: $("#frmSupplierPaymentTerms").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.successOperation) {
                        self.rebuildSupplierPaymentTerms(response.spts);
                        self.setDataTableEvents();
                        self.$AddEditSupplierPaymentTerms.hide();
                        self.$supplierPaymentTermsRecords.show();
                    }
                    else {
                        self.clearErrors();
                        self.populateErrors(response.Errors);
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        },
        clearErrors: function () {
            $("#ptermspaymenttermsError").text("");
            $("#ptermsfiscalyearError").text("");
            $("#ptermsfiscalquarterError").text("");
            $("#pTermsErrorUL").empty();
        },
        populateErrors: function (errors) {
            var self = this;

            $.each(errors, function (key, value) {
                $("#pterms" + key + "Error").text(value).css({ "color": "red", "font-weight": "bold" });//.addClass("font4");;
                if (key == "sptDuplicate") $("#pTermsErrorUL").append("<li>" + value + "</li>");
            });
        }
    };
    var initSupplierPaymentTerms = function () {
        var supplierPaymentTermsObj = Object.create(supplierPaymentTerms);
        supplierPaymentTermsObj.init();
    }
    // end supplier payment terms object

    // supplier expense obj
    var supplierExpense = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
            self.setDataTableEvents();
        },
        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$btnAddSupplierExpense = $("#btnAddSupplierExpense");
            self.$btnAddSupplierExpenseSystemAdmin = $("#btnAddSupplierExpenseSystemAdmin");
            self.$SupplierId = $("#SupplierId").val();
            self.$supplierBUID = $("#supplierBUID").val();
            self.$userLoggedInBUID = $("#userLoggedInBUID").val();
            self.$userLoggedInRoleID = $("#userLoggedInRoleID").val();
            self.$AddEditSupplierExpense = $("#AddEditSupplierExpense");
            self.$supplierExpenseRecords = $("#supplierExpenseRecords");
            self.$supplierExpenseTable = $("#supplierExpenseTable");
            self.$supplierExpenseTbody = $("#supplierExpenseTbody");
            self.$frmSupplierExpense = $("#frmSupplierExpense");
            self.isSystemAdmin = self.$userLoggedInRoleID == 1;
        },
        setEvents: function () {
            var self = this;

            self.$btnAddSupplierExpense.off("click").on("click", function (e) {
                e.preventDefault();
                ShowLoading();
                var deferred = $.Deferred();
                self.AddEditSupplierExpense(0, self.$SupplierId, self.$supplierBUID, self.isSystemAdmin, deferred);
                $.when(deferred).done(function () {
                    HideLoading();
                    $("#btnSaveSupplierExpense").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplierExpense();
                    });
                    $("#btnCancelSupplierExpense").on('click', function (e) {
                        e.preventDefault();
                        self.$AddEditSupplierExpense.hide();
                        self.$supplierExpenseRecords.show();
                    });
                });
            });

            self.$btnAddSupplierExpenseSystemAdmin.off('click').on('click', function (e) {
                e.preventDefault();
                ShowLoading();
                var deferred = $.Deferred();
                self.AddEditSupplierExpense(0, self.$SupplierId, self.$supplierBUID, self.isSystemAdmin, deferred);
                $.when(deferred).done(function () {
                    HideLoading();
                    $("#btnSaveSupplierExpense").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveSupplierExpense();
                    });
                    $("#btnCancelSupplierExpense").on('click', function (e) {
                        e.preventDefault();
                        self.$AddEditSupplierExpense.hide();
                        self.$supplierExpenseRecords.show();
                    });
                });
            });
        },
        setDataTableEvents: function () {
            var self = this;

            self.$supplierExpenseTable.DataTable().destroy();

            self.$supplierExpenseTable.DataTable({
                "aaSorting": [],
                "preDrawCallback": function (settings, json) {
                    $.each(self.$supplierExpenseTbody.find('.a-edit'), function (i, obj) {
                        $(obj).off('click').on('click', function (e) {
                            e.preventDefault();
                            ShowLoading();
                            var deferred = $.Deferred();
                            self.AddEditSupplierExpense($(this).data('id'), self.$SupplierId, $(this).data('buid'), self.isSystemAdmin, deferred);
                            $.when(deferred).done(function () {
                                HideLoading();
                                $("#btnSaveSupplierExpense").on('click', function (e) {
                                    e.preventDefault();
                                    self.processSaveSupplierExpense();
                                });
                                $("#btnCancelSupplierExpense").on('click', function (e) {
                                    e.preventDefault();
                                    self.$AddEditSupplierExpense.hide();
                                    self.$supplierExpenseRecords.show();
                                });
                            });
                        })
                    });
                }
            });
        },
        AddEditSupplierExpense: function (id, supplierID, buid, isSystemAdmin, deferred) {
            var self = this;

            $.ajax({
                type: 'GET',
                url: GetAppPath() + 'SupplierExpense/AddEditSupplierExpense/' + id,
                data: {
                    supplierID: supplierID,
                    buid: buid,
                    isSystemAdmin: isSystemAdmin
                },
                success: function (response) {
                    self.$AddEditSupplierExpense.html(response).show();
                    self.$supplierExpenseRecords.hide();
                    deferred.resolve();
                }
            });
        },
        processSaveSupplierExpense: function () {
            var self = this;

            ShowLoading();

            $.ajax({
                url: GetAppPath() + "SupplierExpense/Post",
                type: "POST",
                data: $("#frmSupplierExpense").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.successOperation) {
                        self.rebuildSupplierExpense(response.ses);
                        self.setDataTableEvents();
                        self.$AddEditSupplierExpense.hide();
                        self.$supplierExpenseRecords.show();
                    }
                    else {
                        self.clearErrors();
                        self.populateErrors(response.Errors);
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        },
        clearErrors: function () {
            $("#expensefiscalyearError").text("");
            $("#expensefiscalquarterError").text("");
            $("#expensesalesvolumeError").text("");
            $("#supplierExpenseErrorUL").empty();
        },
        populateErrors: function (errors) {
            $.each(errors, function (key, value) {
                $("#expense" + key + "Error").text(value).css({ "color": "red", "font-weight": "bold" });//.addClass("font4");;
                if (key == "seDuplicate") $("#supplierExpenseErrorUL").append("<li>" + value + "</li>");
            });
        },
        rebuildSupplierExpense: function (ses) {
            var self = this;

            self.$supplierExpenseTable.DataTable().destroy();

            self.$supplierExpenseTbody.empty();

            $.each(ses, function (i, obj) {
                var firstTd = obj.BusinessUnitID == self.$userLoggedInBUID || self.$userLoggedInRoleID == 1 ? "<td>" + "<a href='#' class='a-edit' data-id='" + obj.ExpenseID + "' data-buid='" + obj.BusinessUnitID + "'>" + obj.BusinessUnit.BUName + "</a>" + "</td>" : "<td class='unclickable'>" + obj.BusinessUnit.BUName + "</td>";
                var htmlTx = "<tr>" +
                    firstTd +
                    "<td>" + obj.FiscalYear.FYDescription + "</td>" +
                    "<td>" + obj.FiscalQuarter.FQDescription + "</td>" +
                    "<td>" + obj.SalesVolume + "</td>" +
                    "</tr>";
                self.$supplierExpenseTbody.append(htmlTx);
            });

            HideLoading();
        }
    };
    var initSupplierExpense = function () {
        var supplierExpenseObj = Object.create(supplierExpense);
        supplierExpenseObj.init();
    }
    // end supplier expense obj
});
