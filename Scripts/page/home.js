﻿$(document).ready(function () {
    $("#accordion").accordion({
        icons: {
            "header": "fa fa-plus",
            "activeHeader": "fa fa-minus"
        }
    });

    var HomeObjTask = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
        },

        declaration: function () {
            var self = this;
            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.supplierBUArray = [];

            self.divSupplierCountByBU = $("#fYTD");
        },

        setEvents: function () {
            var self = this;

            self.getBUData();
        },

        displayBUSupplierHighChart: function () {
            var self = this;
            var dt = new Date();


            Highcharts.chart('fYTD', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Business Unit Supplier Count in Year ' + dt.getFullYear()
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
                },
                plotOptions: {
                    pie: {
                        size: 150,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{ name: 'Supplier', data: self.supplierBUArray}]
            });

        },
        getBUData: function () {
            var self = this;

            $.ajax({
                url: self.pageUrl + "/GetPieChartList",
                type: "GET",
                dataType: 'json',
                success: function (resultData) {
                    $.each(resultData.businessUnitSupplierCountList, function (index, item) {
                        self.supplierBUArray.push({ name: item.Key, y: item.Value })
                    })
                    self.displayBUSupplierHighChart();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        }
    };
    var InitializedHomeObjTask = function () {
        var homeObjTask = Object.create(HomeObjTask);
        homeObjTask.init();
    };

    InitializedHomeObjTask();



});