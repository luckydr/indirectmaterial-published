﻿$(document).ready(function () {

    var SupplierStatusObjTask = {

        init: function () {
            var self = this;
            ShowLoading();
            self.declaration();
            self.setEvent();
        },
        declaration: function () {
            var self = this;
            //Maintenance
            self.$SupplierStatusTable = $('#tblSupplierStatus');

            self.$dataTableSupplierStatus;
        },
        setEvent: function(){
            var self = this;

            self.$dataTableSupplierStatus = self.$SupplierStatusTable.dataTable({
                "order": [[3, "desc"]],
                "initComplete": function (settings, json) {
                    HideLoading();
                }
            });
            
        }
    }


    var InitializedSupplierStatusObjTask = function () {
        var supplierStatusObjTask = Object.create(SupplierStatusObjTask);
        supplierStatusObjTask.init();
    };

    InitializedSupplierStatusObjTask();

});