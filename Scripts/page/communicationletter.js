﻿$(document).ready(function () {
    var communicationLetterObj = {
        init: function () {
            var self = this;

            self.declaration();
            self.setEvents();
        },
        declaration: function () {
            var self = this;

            self.$container = $("div[data-maincontainer-yn]");
            self.pageUrl = GetAppPath() + self.$container.data("page-url");
            self.$dataTableCommunicationLetter;
            self.$btnAdd = $("#btnAddCommunicationLetter");
            self.$communicationLetterTable = $("#communicationLetterTable");
            self.$communicationLetterTableTbody = $("#communicationLetterTbody");
            self.$comRow = $("#comRow");
            self.$successMessage = $("#successMesasage");
            self.$frmCommunicationLetter = $("form#frmCommunicationLetter");
        },
        setEvents: function () {
            var self = this;

            self.$btnAdd.off("click").on("click", function (event) {
                ShowLoading();
                var deferred = $.Deferred();
                $.when(deferred).done(function (partialviewhtmltx) {
                    HideLoading();
                    var afterShowEventFunc = [];

                    afterShowEventFunc.push(function () {
                        $("#btnYes").on('click', function (e) {
                            e.preventDefault();
                            self.processSaveCommunicationLetter();
                        });
                        $("#btnNo").on('click', function (e) {
                            e.preventDefault();
                            self.clearValidationCommunicationLetterForm();
                            $('#CommunicationLetterModal').modal('toggle');
                        });
                    });
                    $(this).modalTask({
                        id: "CommunicationLetterModal",
                        title: "Add Communication Letter",
                        dataHtml: partialviewhtmltx,
                        eventsAfterShow: afterShowEventFunc
                    });
                });
                self.populateDataInCommunicationLetterModal(self.pageUrl + '/AddEditCommunicationLetter/0', deferred);
                event.preventDefault();
            });

            $.each($(".a-edit"), function (i, obj) {
                $(obj).off('click').on('click', function (e) {
                    ShowLoading();
                    var dataID = $(this).data('id');
                    self.editCommunicationLetter(dataID);
                    e.preventDefault();
                })
            });
        },
        editCommunicationLetter: function (id) {
            var self = this;
            var deferred = $.Deferred();
            $.when(deferred).done(function (partialviewhtmltx) {
                HideLoading();
                var afterShowEventFunc = [];

                afterShowEventFunc.push(function () {
                    $("#btnYes").on('click', function (e) {
                        e.preventDefault();
                        self.processSaveCommunicationLetter();
                    });
                    $("#btnNo").on('click', function (e) {
                        e.preventDefault();
                        $('#CommunicationLetterModal').modal('toggle');
                    });
                });

                $(this).modalTask({
                    id: "CommunicationLetterModal",
                    title: "Edit Communication Letter",
                    dataHtml: partialviewhtmltx,
                    eventsAfterShow: afterShowEventFunc
                });
            });
            self.populateDataInCommunicationLetterModal(self.pageUrl + '/AddEditCommunicationLetter/' + id, deferred);
        },
        populateDataInCommunicationLetterModal: function (url, deferred) {
            $.ajax({
                url: url,
                type: "GET",
                success: function (data) {
                    deferred.resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    deferred.reject();
                }
            });
        },
        processSaveCommunicationLetter: function () {
            var self = this;

            ShowLoading();

            $.ajax({
                url: self.pageUrl + "/Post",
                type: "POST",
                data: $("#frmCommunicationLetter").serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.Data) {
                        $("#CommunicationLetterModal").modal("toggle");
                        var refresh = setTimeout(function () {
                            location.reload();
                        }, 1500);
                        Alert("Communication letter is now updated");
                        HideLoading();
                    }
                    else {
                        self.clearValidationCommunicationLetterForm();
                        $.each(response.Errors, function (key, value) {
                            $("#" + key + "Error").text("Please fill the required field").css({ "color": "red", "font-weight": "bold", "margin-left": "10px" });//.addClass("font4");
                        });
                        HideLoading();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideLoading();
                }
            });
        },
        clearValidationCommunicationLetterForm: function () {
            $("#letterError").text("");
            $("#businessunitError").text("");
        },
        rebuildCommunicationLetterTable: function (response, deferred) {
            var self = this;

            self.$comRow.empty();

            HideLoading();

            $.each(response.communicationLetters, function (index, communicationLetter) {
                var html = "<div class='col-xs-12 col-sm-4'> <a href='#' class='a-edit' data-id='" + communicationLetter.LetterID + "'>" + communicationLetter.BusinessUnit.BUName + "</a>"
                + "</div>";

                self.$comRow.append(html);
            });

            deferred.resolve();
        }
    }
    var InitializeCommunicationLetterTask = function () {
        var instance = Object.create(communicationLetterObj);
        instance.init();
    }
    InitializeCommunicationLetterTask();
});